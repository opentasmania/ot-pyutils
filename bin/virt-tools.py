#!/usr/bin/python3
#
# virt-tools.py - libvirt helper
# Copyright (C) Peter Lawler
# Snail mail: PO Box 195
#             Lindisfarne, Tasmania
#             AUSTRALIA 7015
# email:      relwalretep@gmail.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# https://libvirt.org/docs/libvirt-appdev-guide-python/en-US/html/libvirt_application_development_guide_using_python-Storage_Pools.html

from __future__ import print_function
from xml.dom import minidom
from libvirt import open
from time import ctime
import argparse
import sys

parser = argparse.ArgumentParser(description='libvirt helper.')
parser.add_argument('--version', action='version', version='%(prog)s 0.8')
parser.add_argument('--host', dest='domName',
                    nargs=1,
                    help='domain name to act upon',
                    required=True)
domName = parser.parse_args().domName[0]

conn = open('qemu:///system')
if conn is None:
    print('Failed to open connection to qemu:///system', file=sys.stderr)
    exit(1)

try:
    dom = conn.lookupByName(domName)
except:
    print('Failed to find the main domain')
    exit(1)

if dom is None:
    print('Failed to find the domain '+domName, file=sys.stderr)
    exit(1)

id = dom.ID()
if id == -1:
    print('The domain is not running so has no ID.')
else:
    print('The ID of the domain is ' + str(id))

uuid = dom.UUIDString()
print('The UUID of the domain is ' + uuid)

domOStype = dom.OSType()
print('The OS type of the domain is "' + domOStype + '"')

flag = dom.hasCurrentSnapshot()
print('The value of the current snapshot flag is ' + str(flag))

flag = dom.hasManagedSaveImage()
print('The value of the manaed save images flag is ' + str(flag))

# name = dom.hostname()
# print('The hostname of the domain  is ' + str(name))


state, maxmem, mem, cpus, cput = dom.info()
print('The state is ' + str(state))
print('The max memory is ' + str(maxmem))
print('The memory is ' + str(mem))
print('The number of cpus is ' + str(cpus))
print('The cpu time is ' + str(cput))


flag = dom.isActive()
if flag is True:
    print('The domain is active.')
else:
    print('The domain is not active.')

flag = dom.isPersistent()
if flag is True:
    print('The domain is persistent.')
elif flag is False:
    print('The domain is not persistent.')
else:
    print('There was an error checking persistence.')

flag = dom.isUpdated()
if flag is True:
    print('The domain is updated.')
elif flag is False:
    print('The domain is not updated.')
else:
    print('There was an error checking updated.')

mem = dom.maxMemory()
if mem > 0:
    print('The max memory for domain is ' + str(mem) + 'MB')
else:
    print('There was an error determining maximum memory.')

cpus = dom.maxVcpus()
if cpus != -1:
    print('The max Vcpus for domain is ' + str(cpus))
else:
    print('There was an error determining maxVcpus.')

name = dom.name()
print('The name of the domain is "'+name+'".')

state, reason = dom.state()
print('The state is '+str(state))
print('The reason code is ' + str(reason))


# struct = dom.getTime()
# timestamp = ctime(float(struct['seconds']))
# print('The domain current time is ' + timestamp)

# Network
# https://libvirt.org/docs/libvirt-appdev-guide-python/en-US/html/libvirt_application_development_guide_using_python-Guest_Domains-Device_Config-Networking.html
raw_xml = dom.XMLDesc(0)
xml = minidom.parseString(raw_xml)
interfaceTypes = xml.getElementsByTagName('interface')
for interfaceType in interfaceTypes:
    print('interface: type='+interfaceType.getAttribute('type'))
    interfaceNodes = interfaceType.childNodes
    for interfaceNode in interfaceNodes:
        if interfaceNode.nodeName[0:1] != '#':
            print('  '+interfaceNode.nodeName)
            for attr in interfaceNode.attributes.keys():
                print('    '+interfaceNode.attributes[attr].name + ' = ' +
                      interfaceNode.attributes[attr].value)

# Mice, keys, tablets
# https://libvirt.org/docs/libvirt-appdev-guide-python/en-US/html/libvirt_application_development_guide_using_python-Guest_Domains-Device_Config-Mice.html
raw_xml = dom.XMLDesc(0)
xml = minidom.parseString(raw_xml)
devicesTypes = xml.getElementsByTagName('input')
for inputType in devicesTypes:
    print('input: type=' + inputType.getAttribute('type') + ' bus=' +
          inputType.getAttribute('bus'))
    inputNodes = inputType.childNodes
    for inputNode in inputNodes:
        if inputNode.nodeName[0:1] != '#':
            print('  '+inputNode.nodeName)
            for attr in inputNode.attributes.keys():
                print('    '+inputNode.attributes[attr].name + ' = ' +
                      inputNode.attributes[attr].value)


conn.close()
exit(0)
