#!/usr/bin/python3

# send_sms.py - a simple python script to send an SMS message
# Copyright (C) Peter Lawler
# Snail mail: PO Box 195
#             Lindisfarne, Tasmania
#             AUSTRALIA 7015
# email:      relwalretep@gmail.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from __future__ import print_function
# use python-gsmmodem-new for python3.4+ compatibility
from gsmmodem.modem import GsmModem

# import logging
import sys

PORT = '/dev/ttyO4'
BAUDRATE = 115200
PIN = None  # SIM card PIN (if any)
SMS_RECEIVER_NUMBER = "########"

if SMS_RECEIVER_NUMBER == "########":
    print('Error: SMS_RECEIVER_NUMBER not set')
    exit(1)


def main():
    print('Initialising modem')
    # logging.basicConfig(format='%(levelname)s: %(message)s',
    #                    level=logging.DEBUG)
    modem = GsmModem(PORT, BAUDRATE)
    modem.smsTextMode = False
    modem.connect(PIN)
    try:
        if (len(sys.argv) > 1):
                message = ' '.join(sys.argv[1:])
        else:
                message = "generic message"
        print(message)
        modem.sendSms(SMS_RECEIVER_NUMBER, message)
    finally:
        modem.close()


if __name__ == '__main__':
    main()
