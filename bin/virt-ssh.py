#!/usr/bin/python3

# virt-ssh.py - libvirt ssh helper
# Copyright (C) Peter Lawler
# Snail mail: PO Box 195
#             Lindisfarne, Tasmania
#             AUSTRALIA 7015
# email:      relwalretep@gmail.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


from __future__ import print_function
from libvirt import open as lvopen
from subprocess import PIPE, Popen
from xml.dom import minidom
from argparse import ArgumentParser
import sys


parser = ArgumentParser(description='virt ssh.')
parser.add_argument('--version', action='version', version='%(prog)s 0.4')
parser.add_argument('--host', dest='domName',
                    nargs=1,
                    help='domain name to look up',
                    required=True)
parser.add_argument('--user', dest='userName', nargs=1,
                    help='username to connect as',
                    required=False)
parser.add_argument('--remote-command', dest='remoteCmd',
                    nargs=1,
                    help='remote command to execute',
                    required=False)
parser.add_argument('--verbose', dest='verbose',
                    const=True, default=False,
                    action='store_const',
                    help='verbose output',
                    required=False)

args = parser.parse_args()

domName = args.domName[0]
try:
    if args.userName[0]:
        userName = args.userName[0]
except Exception:
    pass

def find_ip(mac):
    """
        Given a host's MAC, find its IP in /proc/net/arp table
    """
    with open('/proc/net/arp') as arps:
        for line in arps:
            if mac not in line:
                continue
            try:
                print(line.split('""')[1])
            except IndexError:
                ip = line.partition(' ')
        return ip[0]


def get_macaddr():
    """
        Retreive MAC address for a given domain
    """
    rawxml = dom.XMLDesc(0)
    xml = minidom.parseString(rawxml)
    ifTypes = xml.getElementsByTagName('interface')
    for ifType in ifTypes:
        if ifType.getAttribute('type') == "network":
            ifNodes = ifType.childNodes
            for ifNode in ifNodes:
                if ifNode.nodeName[0:3] == 'mac':
                    for attr in ifNode.attributes.keys():
                        if ifNode.attributes[attr].name == 'address':
                            macAddr = ifNode.attributes[attr].value
    return macAddr


conn = lvopen('qemu:///system')
if conn is None:
    print('Failed to open connection to qemu:///system', file=sys.stderr)
    exit(1)

try:
    dom = conn.lookupByName(domName)
except:
    print('Failed to find the main domain')
    exit(1)

if dom is None:
    print('Failed to find the domain '+domName, file=sys.stderr)
    exit(1)

id = dom.ID()
if id == -1:
    print('The domain is not running so has no ID.')


macAddr = get_macaddr()
destIP = find_ip(macAddr)
if args.verbose:
    print("MAC " + macAddr)
    print("IP  " + destIP)

try:
    if userName:
        dest = userName + "@" + destIP
except Exception:
    dest = destIP

if args.remoteCmd:
    if args.verbose:
        sshCommand='ssh -v -F /dev/null'
        print('%s %s %s' % (sshCommand,dest,str(args.remoteCmd[0])))
    else:
        sshCommand='ssh -F /dev/null'
    cmd=sshCommand+' '+dest+' '+str(args.remoteCmd[0])
    stream = Popen(cmd, stdin=PIPE, stdout=PIPE,
                   shell=True)
    rsp = stream.stdout.read().decode('utf-8')
    print(rsp)

