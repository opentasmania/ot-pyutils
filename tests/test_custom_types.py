# -*- coding: utf-8 -*-
"""This module test custom types."""

# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

from typing import Union

import pytest

from pyutils.custom_types import JSON, get_json_value


def add_value(data: JSON, value: Union[str, float, bool, None]) -> JSON:
    """Take JSON data and adds a provided value to it.

    If the data is a dictionary, the value is added as a new key-value pair with "new_key" as the key.
    If the data is a list, the value is appended to the end of the list.
    If the data is neither a dictionary nor a list, a TypeError is raised.

    :param data: The JSON data to which the value is to be added. It can be a dictionary or a list.
    :param value: The value to be added to the JSON data. It can be a string, float, boolean or None.
    :return: The modified JSON data after adding the value.

    Example usage:
    ```
    data = {"existing_key": "existing_value"}
    value = "new_value"
    result = add_value(data, value)
    print(result)

    # Output: {'existing_key': 'existing_value', 'new_key': 'new_value'}
    ```
    """
    if isinstance(data, dict):
        data["new_key"] = value
    elif isinstance(data, list):
        data.append(value)
    else:
        raise TypeError("Data should be a dict or a list.")
    return data


def test_add_value_dict():
    """Test method for adding a new value to a dictionary.

    :return: None
    """
    dict_data = {"key": "value"}
    dict_data = add_value(dict_data, "new_value")
    assert dict_data["new_key"] == "new_value"


def test_add_value_list():
    """Test method to check the behaviour of the add_value function when adding a value to a list.

    :return: None
    """
    list_data = [1, 2, 3]
    list_data = add_value(list_data, "new_value")
    assert list_data[-1] == "new_value"


def test_add_value_error():
    """Test TypeError raised when adding a string to a value.

    :return: None
    """
    with pytest.raises(TypeError):
        add_value("string", "new_value")


def test_get_json_value_key_exists():
    """Test method for get_json_value when the key exists in the JSON object.

    :return: None
    """
    json_object = {"name": "AI Assistant", "age": 1}
    key = "name"
    expected_value = "AI Assistant"
    assert get_json_value(json_object, key) == expected_value


def test_get_json_value_key_not_found():
    """Test get_json_value returns None when the key is not found in the JSON object.

    Parameters:
        None

    Returns:
        None

    Raises:
        AssertionError: If the get_json_value does not return None when the key is not found.

    Example Usage:
        test_get_json_value_key_not_found()

    """
    json_object = {"name": "AI Assistant", "age": 1}
    key = "job"
    assert get_json_value(json_object, key) is None


def test_get_json_value_with_non_dict():
    """est the functionality of the `get_json_value` with non-dictionary JSON object.

    Parameters:
    - None

    Raises:
    - `ValueError`: If the provided JSON object is not a dictionary.

    Returns:
    - None

    Example usage:
    ```python
    test_get_json_value_with_non_dict()
    ```

    """
    json_object = ["AI Assistant", 1]
    key = "name"
    with pytest.raises(
        ValueError, match="The provided JSON object is not a dictionary."
    ):
        get_json_value(json_object, key)


def test_get_json_value_with_nested_json():
    """Test the get_json_value function with nested JSON.

    The function tests the behavior of the get_json_value function when given a JSON object with nested values.

    :returns: None

    Example usage:
        test_get_json_value_with_nested_json()
    """
    json_object = {"person": {"name": "AI Assistant", "age": 1}}
    key = "name"
    assert get_json_value(json_object, key) is None

    key = "person"
    expected_value = {"name": "AI Assistant", "age": 1}
    assert get_json_value(json_object, key) == expected_value
