# -*- coding: utf-8 -*-
"""
    Test routines for hashes
"""
from pyutils import hash_processor
import hashlib


def test_hash_processor_initialization():
    hash_processor_obj = hash_processor.HashProcessor(algorithm="md5")

    assert hash_processor_obj.algorithm == "md5"


def test_update_method():
    hash_processor_obj = hash_processor.HashProcessor(algorithm="md5")
    hash_processor_obj.update("Hello, World!")

    assert hash_processor_obj._hash_object is not None


def test_hexdigest_method():
    hash_processor_obj = hash_processor.HashProcessor(algorithm="md5")
    hash_processor_obj.update("Hello, World!")

    assert (
        hash_processor_obj.hexdigest() == "65a8e27d8879283831b664bd8b7f0ad4"
    )


def test_reset_method():
    hash_processor_obj = hash_processor.HashProcessor(algorithm="sha1")
    hash_processor_obj.update("Hello, World!")
    hash_processor_obj.reset()

    assert (
        hash_processor_obj._hash_object.hexdigest()
        == hashlib.new("sha1").hexdigest()
    )


def test_compare_method():
    text = "Hello, World!"
    hashed_value = hashlib.sha256(text.encode("utf-8")).hexdigest()

    hash_processor_obj = hash_processor.HashProcessor(algorithm="sha256")
    assert hash_processor_obj.compare(text, hashed_value) is True

    wrong_hashed_value = hashlib.sha256(
        (text + "123").encode("utf-8")
    ).hexdigest()
    assert hash_processor_obj.compare(text, wrong_hashed_value) is False
