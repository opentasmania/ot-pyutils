# -*- coding: utf-8 -*-
"""This module tests config arguments."""

# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

import sys
from unittest.mock import patch

import pytest

from pyutils import args

default_name = "unnamed"


def test_print_help_message():
    """
    Test the print_help_message method.

    :return: None
    """
    default_root = "/test/root/path"
    default_config = "/test/config/path"
    expected_result = (
        f"Parameters defaults:\n"
        f"--name={default_name}\n"
        f"--root={default_root}\n"
        f"--config={default_config}\n"
        "Usage:\n"
        "--root=<root_path> --config=<config_path>"
    )

    with patch("builtins.print") as mock_print:
        args.print_help_message(default_root, default_config)

    mock_print.assert_called_once_with(expected_result)


def test_assign_parameter_from_arguments_no_args():
    """Verify the behaviour of assign_parameter_from_arguments() method when no arguments are provided.

    :return: None
    """
    sys.argv = ["program"]
    name, root_path, config_path = args.assign_parameter_from_arguments()
    default_root, default_config = args.default_directories()

    assert name == default_name
    assert root_path == default_root
    assert config_path == default_config


def test_assign_parameter_from_arguments_normal():
    """Test method for assign_parameter_from_arguments() function when long arguments are provided.

    :return: None
    """
    sys.argv = ["program", "--name=name", "--root=path1", "--config=path2"]
    result = args.assign_parameter_from_arguments()
    assert result == (
        "name",
        "path1",
        "path2",
    ), f'Expected ("name","path1", "path2"), got {result}'


def test_assign_parameter_from_arguments_short():
    """Test method for assign_parameter_from_arguments() function when short arguments are provided.

    :return: None
    """
    sys.argv = ["program", "-n=name", "-r=path1", "-c=path2"]
    result = args.assign_parameter_from_arguments()
    assert result == (
        "name",
        "path1",
        "path2",
    ), f'Expected ("name","path1", "path2"), got {result}'


def test_assign_parameter_from_arguments_only_name():
    """Test assign_parameter_from_arguments() method when only the name argument is provided as an argument.

    :return: None

    """
    sys.argv = ["program", "--name="]
    with pytest.raises(SystemExit):
        args.assign_parameter_from_arguments()


def test_assign_parameter_from_arguments_only_root():
    """Test assign_parameter_from_arguments() method when only the root argument is provided as an argument.

    :return: None
    """
    sys.argv = ["program", "--root="]
    with pytest.raises(SystemExit):
        args.assign_parameter_from_arguments()


def test_assign_parameter_from_arguments_only_config():
    """Test assign_parameter_from_arguments() method when only the config argument is provided as an argument.

    :return: None

    """
    sys.argv = ["program", "--config="]
    with pytest.raises(SystemExit):
        args.assign_parameter_from_arguments()


def test_assign_parameter_from_arguments_no_value():
    """Test method for assigning parameters from arguments when no value is provided.

    :return: None
    """
    sys.argv = ["program", "--name=", "--root=", "--config="]
    with pytest.raises(SystemExit):
        args.assign_parameter_from_arguments()
