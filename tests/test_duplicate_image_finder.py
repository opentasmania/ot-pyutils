from unittest.mock import patch, Mock

import pytest
from PIL import Image, UnidentifiedImageError

from pyutils.duplicate_image_finder import calculate_image_hash


def test_calculate_image_hash_successful():
    """Test calculate_image_hash executes successfully."""
    with patch("pyutils.duplicate_image_finder.Image.open") as mocked_open:
        mocked_open.return_value = Image.new("RGB", (60, 30), color="red")
        assert (
            calculate_image_hash("path/to/test/image.jpg", "mode", 0)
            is not None
        )


def test_calculate_image_hash_unsupported_format():
    """Test calculate_image_hash for unsupported image format."""
    with patch(
        "pyutils.duplicate_image_finder.Image.open",
        side_effect=UnidentifiedImageError,
    ):
        assert (
            calculate_image_hash("path/to/test/image.unsupported", "mode", 0)
            is None
        )


def test_calculate_image_hash_with_exception():
    """Test calculate_image_hash for any other exception."""
    with patch(
        "pyutils.duplicate_image_finder.Image.open", side_effect=Exception
    ):
        assert (
            calculate_image_hash("path/to/test/image.jpg", "mode", 0) is None
        )


@pytest.mark.skip(reason="Skip this test for now")
def test_calculate_image_hash_for_animation_single_mode():
    """Test calculate_image_hash for single mode."""
    mocked_image = Mock()
    mocked_image.is_animated = True
    mocked_image.n_frames = 2
    mocked_image.seek = Mock()

    with patch(
        "pyutils.duplicate_image_finder.Image.open", return_value=mocked_image
    ):
        assert (
            calculate_image_hash("path/to/test/image.gif", "single", 0)
            is not None
        )


@pytest.mark.skip(reason="Skip this test for now")
def test_calculate_image_hash_for_animation_whole_mode():
    """Test calculate_image_hash for whole mode."""
    mocked_image = Mock()
    mocked_image.is_animated = True
    mocked_image.n_frames = 2
    with patch(
        "pyutils.duplicate_image_finder.Image.open", return_value=mocked_image
    ):
        assert (
            calculate_image_hash("path/to/test/image.gif", "whole", 0)
            is not None
        )


@pytest.mark.skip(reason="Skip this test for now")
def test_calculate_image_hash_frame_does_not_exist():
    """Test calculate_image_hash where frame does not exist."""
    mocked_image = patch.Mock()
    mocked_image.is_animated = True
    mocked_image.n_frames = 1
    with patch(
        "pyutils.duplicate_image_finder.Image.open", return_value=mocked_image
    ):
        assert (
            calculate_image_hash("path/to/test/image.gif", "single", 5)
            is None
        )


@pytest.mark.skip(reason="Skip this test for now")
def test_calculate_image_hash_dng_format():
    """Test calculate_image_hash for dng format."""
    mocked_image = Mock()
    with patch(
        "pyutils.duplicate_image_finder.imread", return_value=mocked_image
    ):
        assert (
            calculate_image_hash("path/to/test/image.dng", "mode", 0)
            is not None
        )


@pytest.mark.skip(reason="Skip this test for now")
def test_calculate_image_hash_tif_format():
    """Test calculate_image_hash for tif format."""
    mocked_image = Mock()
    with patch(
        "pyutils.duplicate_image_finder.TiffFile", return_value=mocked_image
    ):
        assert (
            calculate_image_hash("path/to/test/image.tif", "mode", 0)
            is not None
        )
