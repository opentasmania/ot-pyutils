# -*- coding: utf-8 -*-
"""This module test date manipulation functions."""

# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

import unittest
from datetime import datetime, timezone

import pytest
from pytz import utc

from pyutils import date as ot_date


class TestDateValidation(unittest.TestCase):
    """TestDateValidation class for testing the is_date method in the ot_date module."""

    def test_is_date_valid(self):
        """
        Test if the is_date method correctly identifies a valid date.
        """
        self.assertTrue(
            ot_date.is_date("2020-02-20T14:28:23"),
            "Failed on case: Valid date",
        )

    def test_is_date_invalid_format(self):
        """
        Test if the is_date method correctly identifies invalid date formats.
        """
        self.assertFalse(
            ot_date.is_date("202020-02-02T14:28:23"),
            "Failed on case: Year format incorrect",
        )

    def test_is_date_invalid_month(self):
        """
        Check if the month is out of range in the given date.
        """
        self.assertFalse(
            ot_date.is_date("2020-20-02T14:28:23"),
            "Failed on case: Month out of range",
        )

    def test_is_date_invalid_day(self):
        """
        Test if the 'is_date' method returns False when the provided date has an invalid day.
        """
        self.assertFalse(
            ot_date.is_date("2020-02-30T14:28:23"),
            "Failed on case: Day out of range",
        )

    def test_is_date_empty_string(self):
        """
        Test whether the given date string is empty or not.
        """
        self.assertFalse(ot_date.is_date(""), "Failed on case: Empty string")

    def test_is_date_invalid_datetime(self):
        """
        Test if the given string is an invalid datetime.
        """
        self.assertFalse(
            ot_date.is_date("Hello, world!"),
            "Failed on case: Non-datetime string",
        )

    def test_is_date_invalid_delimiter(self):
        """
        Test case for the is_date method when the date string has an incorrect delimiter.
        """
        self.assertFalse(
            ot_date.is_date("2020/02/02 14:28:23"),
            "Failed on case: Incorrect delimiter",
        )


def test_format_datetime_valid_inputs():
    test_datetime = datetime(2024, 2, 1, 12, 0)
    fmt = "%Y-%m-%d %H:%M"
    assert ot_date.format_datetime(test_datetime, fmt) == "2024-02-01 12:00"


def test_format_datetime_invalid_datetime():
    with pytest.raises(TypeError):
        ot_date.format_datetime("invalid_datetime", "%Y-%m-%d %H:%M")


def test_format_datetime_invalid_format():
    test_datetime = datetime(2024, 2, 1, 12, 0)
    with pytest.raises(ValueError):
        ot_date.format_datetime(test_datetime, "invalid_format")


def test_format_datetime_none_datetime():
    with pytest.raises(TypeError):
        ot_date.format_datetime(None, "%Y-%m-%d %H:%M")


def test_format_datetime_none_format():
    test_datetime = datetime(2024, 2, 1, 12, 0)
    with pytest.raises(TypeError):
        ot_date.format_datetime(test_datetime, None)


def test_localize_datetime():
    # Testing with a naive datetime object
    naive_dt = datetime.utcnow()
    assert naive_dt.tzinfo is None
    localized_naive_dt = ot_date.localize_datetime(naive_dt)
    assert localized_naive_dt.tzinfo == utc  # This line has been corrected

    # Testing with an aware datetime object (utc)
    aware_dt_utc = datetime.utcnow().replace(tzinfo=utc)
    localized_aware_dt_utc = ot_date.localize_datetime(aware_dt_utc)
    assert aware_dt_utc == localized_aware_dt_utc

    # Testing with an aware datetime object (non-utc timezone)
    tz = timezone.utc
    aware_dt_non_utc = datetime(2024, 2, 3, 4, 5, 6, tzinfo=tz)
    localized_aware_dt_non_utc = ot_date.localize_datetime(aware_dt_non_utc)
    assert aware_dt_non_utc == localized_aware_dt_non_utc


if __name__ == "__main__":
    unittest.main()
