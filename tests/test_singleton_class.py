# -*- coding: utf-8 -*-
"""This module contains tests for the singleton class."""

# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

from pyutils.singleton_class import Singleton


class _SingletonTestClass(metaclass=Singleton):
    pass


def test_singleton_class_creates_only_single_instance():
    """Test if Singleton class correctly creates only single instance."""
    instance1 = _SingletonTestClass()
    instance2 = _SingletonTestClass()
    assert instance1 is instance2


def test_singleton_class_creates_separate_instances_for_different_classes():
    """Test if Singleton class correctly creates separate instances for different classes."""

    class _AnotherTestClass(metaclass=Singleton):
        pass

    instance1 = _SingletonTestClass()
    instance2 = _AnotherTestClass()

    assert instance1 is not instance2


def test_singleton_class_call():
    """Test the behavior of singleton classes.

    :return: None
    """

    class MyClass(metaclass=Singleton):
        pass

    instance1 = MyClass()
    instance2 = MyClass()

    assert instance1 is instance2

    class AnotherClass(metaclass=Singleton):
        pass

    another_instance = AnotherClass()

    assert instance1 is not another_instance
