"""Test error handling."""

# -*- coding: utf-8 -*-
# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

import pytest

from pyutils.error_handling import with_retry
from pyutils.exceptions import MaxRetriesReached


# Successful Operation
def test_with_retry_success():
    """Test the functionality of the 'with_retry' function when the operation is successful.

    :return: None
    """

    def success_operation():
        """
        Perform a successful operation.

        :return: The string 'success'.
        """
        return "success"

    result = with_retry(success_operation)
    assert result == "success", "Successful callable should return 'success'"


# Unrecoverable failure
def test_with_retry_unrecoverable_failure():
    """
    Test the behavior of a function that retries an unrecoverable failure.

    This function tests the behavior of a function that performs an operation that always fails.
    It uses the `always_fail_operation` function to simulate the failure condition. The `always_fail_operation`
    function raises a `ValueError` exception when called.

    The test uses the `with_retry` function with the `always_fail_operation` function as the operation to be retried.
    It configures the `with_retry` function to catch `ValueError` exceptions and specify a maximum number of attempts
    of 3 using the `max_attempts` parameter.

    The test expects the `with_retry` function to raise a `MaxRetriesReached` exception after 3 failed attempts
    to execute the `always_fail_operation` function.

    :raises MaxRetriesReached: if the `with_retry` function does not raise a `MaxRetriesReached` exception.
    """

    def always_fail_operation():
        """
        Perform an operation that always fails.

        :raises ValueError: if the operation fails.
        """
        raise ValueError("I always fail!")

    with pytest.raises(MaxRetriesReached):
        with_retry(always_fail_operation, (ValueError,), max_attempts=3)


# Recoverable failure
def test_with_retry_recoverable_failure():
    """A method used to test the `with_retry` function with a recoverable failure scenario.

    `failing_first_time` is a sub-method which simulates a failing first time and successful subsequent calls.
     It increases the `call_counter` variable with each call.
     If `call_counter` is equal to 1, it raises a `ValueError` to simulate a failure.
     Otherwise, it returns the string "Success".

    The `with_retry` function is called with `failing_first_time` as the first argument,
     a tuple of `ValueError` as the second argument (to specify the exceptions to catch),
     and a `max_attempts` parameter of 3.

    The expected behavior is that `with_retry` will attempt to call `failing_first_time` up to 3 times,
     catching `ValueError` exceptions,
     and finally return "Success" if the method is called successfully after the first failure.

    :return: None
    """
    call_counter = 0

    def failing_first_time():
        """
        Call this method to simulate a failing first time and successful subsequent calls.

        :return: The string "Success" if the method is called successfully after the first failure.

        :raises ValueError: If the method is called for the first time, it will raise a ValueError.

        """
        nonlocal call_counter
        call_counter += 1
        if call_counter == 1:
            raise ValueError("I fail at first call!")
        else:
            return "Success"

    result = with_retry(failing_first_time, (ValueError,), max_attempts=3)
    assert (
        result == "Success"
    ), "Should recover from first exception and return success after the second call"


def test_with_retry_wait_time_increase(mocker):
    """

    :param mocker: The mocker object used for patching the `pyutils.error_handling.sleep`.

    :return: None.

    """
    mock_sleep = mocker.patch("pyutils.error_handling.sleep")

    def always_fail_operation():
        """
        Perform an operation that always fails.

        :raises ValueError: If the operation fails.
        :return: None.
        """
        raise ValueError("I always fail!")

    with pytest.raises(MaxRetriesReached):
        with_retry(always_fail_operation, (ValueError,), max_attempts=3)
    assert mock_sleep.call_args_list == [
        mocker.call(1),
        mocker.call(2),
        mocker.call(3),
    ], "Should correctly increase the wait time"
