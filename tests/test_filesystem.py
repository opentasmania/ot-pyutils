# -*- coding: utf-8 -*-
"""Test filesystem utilities."""
# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

from collections import defaultdict
from datetime import datetime, timezone
from hashlib import sha512
from os import listdir, makedirs, mkdir, remove
from os.path import isfile, isdir, join, exists
from pathlib import Path
from pytz import utc
from shutil import rmtree
from tempfile import NamedTemporaryFile, TemporaryDirectory, mkdtemp
from unittest import mock
from unittest.mock import Mock, patch

import pytest

from pyutils import filesystem
from pyutils.filesystem import (
    calculate_sha512,
    cleanup_directory,
    clear_directory,
    copy_directory_structure,
    discover_files_checksums,
    get_existing_files,
    get_existing_subdirs,
    get_newest_file_timestamp,
    rename_file_on_latest_timestamp_in_directory,
    validate_filepath,
)

# Sample data
SAMPLE_FILE_PATH = Path("/path/to/root/file.txt")
SAMPLE_ROOT_DIR = Path("/path/to/root")
SAMPLE_CHECKSUM = "dummychecksum"
SAMPLE_DATETIME = datetime(2023, 1, 1, 12, 0)


@pytest.fixture
def temp_file():
    """Create a temporary file.

    Returns:
        str: The name of the temporary configuration file.

    Raises:
        None

    Example:
        filename = temp__file()
        print(filename)
        '/path/to/temporary_file.txt'
    """
    with NamedTemporaryFile(delete=False, suffix=".txt") as tmp:
        tmp.write("Test data".encode())
        tmp.flush()
        yield tmp.name


@pytest.fixture
def temp_dir():
    directory = mkdtemp()
    yield directory
    if isdir(directory):
        rmtree(directory)


def test_validate_filepath(temp_file):
    """Ensure a filepath is valid.

    Args:
        temp_file: The filepath to be validated.

    """
    assert validate_filepath(temp_file) is True


def test_get_existing_files_empty_directory(tmpdir):
    """Test the functionality of `files.get_existing_files` method when specified directory is empty.

    It verifies that the method returns an empty list when there are no existing files in the directory.
    The `tmpdir` parameter is a `py._path.local.LocalPath` object representing the temporary directory to be tested.
    It is used as an input to the `files.get_existing_files` method.

    :param tmpdir: The path to the temporary directory to be tested.
    :return: None.

    Example usage:

    ```python
    test_get_existing_files_empty_directory(tmpdir)
    ```
    """
    assert len(listdir(tmpdir)) == 0
    result = get_existing_files(str(tmpdir))
    assert result == []


def test_get_existing_files_non_empty_directory(tmpdir):
    """Test to get the existing files in a non-empty directory.

    :param tmpdir: Temporary directory to simulate a non-empty directory.
    :return: None

    """
    p = tmpdir.join("hello.txt")
    p.write("content")
    result = get_existing_files(str(tmpdir))
    assert len(result) == 1


def test_get_existing_subdirs_non_empty_directory(tmpdir):
    """Test "get_existing_subdirs" function from the "files" module in a non-empty directory scenario.

    Create a subdirectory named "sub" within the given temporary directory. Then, call "get_existing_subdirs" function,
     passing the temporary directory as an argument.
    Finally, it asserts that the returned list of subdirectories has a length of 1.

    :param tmpdir: A pytest fixture providing a temporary directory for testing purposes.
    :return: None

    """
    tmpdir.mkdir("sub")
    result = get_existing_subdirs(tmpdir)
    assert len(result) == 1


def test_get_existing_files_non_existing_directory():
    """Ensures `get_existing_files` raises a `FileNotFoundError` exception when given a non-existing directory path.

    :return: None
    """
    with pytest.raises(FileNotFoundError):
        get_existing_files("/non/existing/path")


def test_get_existing_files_with_subdirectories(tmpdir):
    """Test get_existing_files_with_subdirectories method.

    :param tmpdir: The temporary directory used for testing.
    :return: None.

    """
    sub_dir = tmpdir.mkdir("sub")
    sub_dir.join("hello.txt").write("content")
    tmpdir.join("world.txt").write("content")
    result = get_existing_files(str(tmpdir))
    assert len(result) == 1
    assert "world.txt" in result[0]


def test_get_existing_files_with_hidden_files(tmpdir):
    """Test 'get_existing_files' method.

    Ensure it correctly returns the list of existing files, including hidden files, in the given directory.

    :param tmpdir: A pytest fixture that provides a temporary directory for testing.
    :return: None
    """
    tmpdir.join(".hidden.txt").write("content")
    result = get_existing_files(str(tmpdir))
    assert len(result) == 1
    assert ".hidden.txt" in result[0]


@pytest.fixture(scope="module")
def setup_test_environment():
    """
    This function is a pytest fixture that sets up the test environment for testing functionality related to file manipulation. It creates a directory structure with a test file inside and yields control to the test functions. After the tests are completed, it cleans up the test environment by removing the created directory.
    """
    makedirs("test_dir/source_dir/sub_dir", exist_ok=True)
    with open("test_dir/source_dir/sub_dir/test_file.txt", "w") as f:
        f.write("Test")
    yield
    rmtree("test_dir")


def test_copy_directory_structure(setup_test_environment):
    """
    Copies the directory structure from a source directory to a destination directory.
    """
    destination_dir = "test_dir/destination_dir"
    copy_directory_structure("test_dir/source_dir", destination_dir)
    assert exists(destination_dir)
    assert exists(join(destination_dir, "sub_dir"))
    assert isfile(join(destination_dir, "sub_dir", "test_file.txt"))


def test_copy_directory_structure_with_nonexistent_source_dir(
    setup_test_environment,
):
    """
    Test case for the `copy_directory_structure` function in the `files` module.
    """
    with pytest.raises(FileNotFoundError):
        copy_directory_structure(
            "test_dir/nonexistent_dir", "test_dir/destination_dir"
        )


def test_clear_directory_existing_directory():
    """
    Test function for clear_directory that checks if an existing directory is properly cleared of all files and subdirectories.

    Creates a temporary directory structure with files and subdirectories, then calls the
    clear_directory method to remove all contents and asserts that the directory is empty.

    This is achieved as follows:
    1. A temporary directory is created using TemporaryDirectory().
    2. Inside this temporary directory, multiple files and a subdirectory with a file are created.
    3. The clear_directory method is called to clear the contents of the temporary directory.
    4. Asserts that the directory is empty after clearing.
    """
    with TemporaryDirectory() as tmp_dir:
        # Create some files and directories inside the temporary directory
        sub_dir = join(tmp_dir, "sub")
        mkdir(sub_dir)
        file1, file2, file3 = [
            join(tmp_dir, f"test{i + 1}.txt") for i in range(3)
        ]
        sub_file = join(sub_dir, "sub_file.txt")
        for file in [file1, file2, file3, sub_file]:
            with open(file, "w") as f:
                f.write("Test")

        clear_directory(tmp_dir)

        assert len(listdir(tmp_dir)) == 0


def test_clear_directory_non_existing_directory():
    """
    Tests the `clear_directory` function with a non-existing directory.
    This test verifies that the function raises a FileNotFoundError
    when attempting to clear a directory that does not exist.
    """
    non_existing_dir = "/non/existing/directory/path"
    with pytest.raises(FileNotFoundError):
        clear_directory(non_existing_dir)


def test_clear_directory_access_error():
    """
    Test the clear_directory function to ensure it raises a PermissionError
    when attempting to delete a file without proper permissions.

    Creates a temporary directory and a file within it.
    Sets file permissions to read-only to trigger a PermissionError upon deletion.
    Asserts that the clear_directory function raises the expected PermissionError.
    Finally, resets the file permissions to allow cleanup of the temporary directory.
    """
    try:
        # Create a temporary directory
        with TemporaryDirectory() as tmpdirname:
            test_file_path = join(tmpdirname, "test.txt")
            # Create a test file
            with open(test_file_path, "w") as f:
                f.write("some content")

            # Ensure the file exists before proceeding
            assert isfile(
                test_file_path
            ), f"File was not created at {test_file_path}"

            # Call the method under test
            cleanup_directory(tmpdirname)

            # Check if the directory was removed
            assert not isdir(
                tmpdirname
            ), f"Directory was not removed: {tmpdirname}"

        # Now test the FileNotFoundError scenario
        with pytest.raises(FileNotFoundError):
            cleanup_directory("/non/existent/directory/path")

    except Exception as e:
        pytest.fail(f"Test failed due to an unexpected error: {e}")


def test_get_newest_file_timestamp_no_path():
    with pytest.raises(FileNotFoundError):
        get_newest_file_timestamp(Path("non-existing-path"))


def test_get_newest_file_timestamp_path_is_not_dir():
    with pytest.raises(NotADirectoryError):
        get_newest_file_timestamp(Path(__file__))


def test_get_newest_file_timestamp_empty_dir(tmp_path: Path):
    assert get_newest_file_timestamp(tmp_path) is None


def test_get_newest_file_timestamp_included_file(tmp_path: Path):
    for i in range(5):
        (tmp_path / f"file{i}").touch()
    newest_file = tmp_path / "newest_file"
    newest_file.touch()

    included_files = [newest_file]

    assert get_newest_file_timestamp(
        tmp_path, include=included_files
    ) == datetime.fromtimestamp(newest_file.stat().st_mtime, tz=utc)


def test_get_newest_file_timestamp_excluded_file(tmp_path: Path):
    for i in range(5):
        (tmp_path / f"file{i}").touch()

    newest_file = tmp_path / "newest_file"
    newest_file.touch()

    excluded_files = [newest_file.resolve()]

    result = get_newest_file_timestamp(tmp_path, exclude=excluded_files)
    expected_time = datetime.fromtimestamp(newest_file.stat().st_mtime)

    assert result is not None
    assert result is not expected_time


def test_calculate_sha512():
    with NamedTemporaryFile(delete=False) as f:
        f.write(b"Hello, World!")
        file_path = Path(f.name)
    f.close()
    try:
        assert (
            calculate_sha512(file_path)
            == sha512(b"Hello, World!").hexdigest()
        )
    finally:
        remove(file_path)


def test_sha512_file_not_existing():
    with pytest.raises(FileNotFoundError):
        calculate_sha512(Path("/nonexistingfile"))


def test__sha512_with_directory():
    with pytest.raises(IsADirectoryError):
        calculate_sha512(Path("/"))


def test_cleanup_directory(temp_dir):
    assert isdir(temp_dir) is True
    cleanup_directory(temp_dir)
    assert isdir(temp_dir) is False, f"Directory {temp_dir} was not removed"


# Test case to handle the exception when directory does not exist
def test_cleanup_non_existent_directory():
    non_existent_dir = "/nonexistent/directory"
    with pytest.raises(FileNotFoundError):
        cleanup_directory(non_existent_dir)


def test_rename_file_on_latest_directory_contents_date():
    with mock.patch(
        "pyutils.filesystem.get_newest_file_timestamp"
    ) as mock_get_newest_file:
        mock_get_newest_file.return_value = datetime(
            2020, 10, 1, 10, 20, 30, tzinfo=timezone.utc
        )
        with mock.patch(
            "pyutils.filesystem.cleanup_directory"
        ) as mock_cleanup_directory:
            mock_cleanup_directory.return_value = None

            test_file_path = "/mydir/test.txt"
            test_directory_name = "mydir"
            test_prefix = "prefix"

            result = rename_file_on_latest_timestamp_in_directory(
                test_file_path, test_directory_name, test_prefix
            )
            date_str = mock_get_newest_file.return_value.strftime(
                "%Y%m%d%H%M%S"
            )
            expected_result = f"{test_prefix}_{Path(test_file_path).stem}_{date_str}{Path(test_file_path).suffix}"
            assert result == expected_result


def test_get_creation_time_file_exists_and_is_file():
    mock_path = Mock(spec=Path)
    mock_path.exists.return_value = True
    mock_path.is_file.return_value = True
    mock_path.stat().st_mtime = 1633078941.9312668
    assert filesystem.get_creation_time(mock_path) == datetime.fromtimestamp(
        1633078941.9312668
    )


def test_get_creation_time_file_not_exists():
    mock_path = Mock(spec=Path)
    mock_path.exists.return_value = False
    with pytest.raises(
        FileNotFoundError, match=f"The file at {mock_path} does not exist"
    ):
        filesystem.get_creation_time(mock_path)


def test_get_creation_time_exists_but_not_file():
    mock_path = Mock(spec=Path)
    mock_path.exists.return_value = True
    mock_path.is_file.return_value = False
    with pytest.raises(
        ValueError, match=f"The path {mock_path} is not a file"
    ):
        filesystem.get_creation_time(mock_path)


def test_get_creation_time_os_error_due_to_insufficient_access():
    mock_path = Mock(spec=Path)
    mock_path.exists.return_value = True
    mock_path.is_file.return_value = True
    mock_path.stat.side_effect = OSError()
    with pytest.raises(
        OSError, match=f"Could not access the file at {mock_path}. Reason:"
    ):
        filesystem.get_creation_time(mock_path)


def test_set_creation_time_file_not_found():
    with pytest.raises(FileNotFoundError):
        non_existent_path = Path("/no/such/file")
        filesystem.set_creation_time(non_existent_path, datetime.now())


def test_set_creation_time_path_not_a_file():
    with pytest.raises(ValueError):
        with patch.object(Path, "exists", return_value=True):
            with patch.object(Path, "is_file", return_value=False):
                invalid_file_path = Path("/this/is/not/a/file")
                filesystem.set_creation_time(
                    invalid_file_path, datetime.now()
                )


@patch("pyutils.filesystem.calculate_sha512", return_value=SAMPLE_CHECKSUM)
@patch("pyutils.filesystem.get_creation_time", return_value=SAMPLE_DATETIME)
@patch("pyutils.filesystem.Path.glob", return_value=[SAMPLE_FILE_PATH])
@patch("pyutils.filesystem.Path.is_file", return_value=True)
def test_discover_files_checksums_basic(
    mock_is_file, mock_glob, mock_creation_time, mock_checksum
):
    # Perform the function call
    result = discover_files_checksums(SAMPLE_ROOT_DIR, Path("file.txt"))

    expected = defaultdict(list)
    expected[SAMPLE_CHECKSUM].append((SAMPLE_FILE_PATH, SAMPLE_DATETIME))

    # Assertions
    assert result == expected, f"Expected {expected}, but got {result}"
    assert mock_glob.called, "glob was not called"
    assert mock_checksum.called, "calculate_sha512 was not called"
    assert mock_creation_time.called, "get_creation_time was not called"
    assert mock_is_file.called, "is_file was not called"
