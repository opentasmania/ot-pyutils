"""Test exceptions."""

# -*- coding: utf-8 -*-
# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

import pytest

from pyutils.exceptions import (
    ExistingFilesException,
    JsonCustomObjectError,
    JsonDateError,
    JsonNumberPrecisionError,
    MaxRetriesReached,
    ModuleNotAvailableException,
    RedTextException,
    RED_TEXT,
    RESET_COLOR,
)


def test_red_text_exception():
    """Tests the exception raised by RedTextException.

    :return: None
    """
    with pytest.raises(RedTextException) as e_info:
        raise RedTextException("This is a test error")
    assert str(e_info.value) == f"{RED_TEXT}This is a test error{RESET_COLOR}"


def test_existing_files_exception_default_message():
    """Test the default error message for ExistingFilesException.

    :return: None
    """
    efe = ExistingFilesException()
    assert str(efe) == "Existing files."


def test_existing_files_exception_custom_message():
    """Test method for creating an instance of ExistingFilesException with a custom message.

    :return: None
    """
    custom_msg = "Files already exist in the directory."
    efe = ExistingFilesException(message=custom_msg)
    assert str(efe) == custom_msg


def test_existing_files_exception_type():
    """Test method for ExistingFilesException.

    Description:
    This method tests the behavior of the 'ExistingFilesException' exception type.
    It verifies that an instance of 'ExistingFilesException' is raised when an exception is intentionally triggered.

    :return: None
    """
    with pytest.raises(ExistingFilesException):
        raise ExistingFilesException()


def test_module_no_available_exception_init():
    """
    Initializes a new instance of the `ModuleNotAvailableException` class.

    :return: None
    """
    message = "Custom message"
    exc = ModuleNotAvailableException(message)
    assert exc.message == message


def test_module_no_available_exception_str():
    """Test the __str__ method of the ModuleNotAvailableException class.

    :return: None

    """
    message = "Custom message"
    exc = ModuleNotAvailableException(message)
    str_exc = str(exc)
    assert str_exc == message


def test_module_not_available_exception_default():
    """Test the default message of ModuleNotAvailableException.

    :return: None
    """
    default_message = "Module cannot be installed or imported."
    exc = ModuleNotAvailableException()
    assert str(exc) == default_message


def test_MaxRetriesReached_with_retries_and_cause():
    """Test the behavior of the `MaxRetriesReached` exception.

     When it is initialized with the number of retries and the cause of the exception.

    :return: None
    """
    retries = 5
    cause = "test_case: invalid auth"
    e = MaxRetriesReached(retries, cause)
    assert e.retries == retries, f"Expected {retries} but found {e.retries}"
    assert e.cause == cause, f"Expected {cause} but found {e.cause}"
    assert (
        e.message
        == f"Maximum number of retries ({retries}) has been exceeded. Cause: {cause}."
    )


def test_MaxRetriesReached_with_retries_and_no_cause():
    """Test the MaxRetriesReached exception when retries are specified but no cause is provided.

    :return: None
    """
    retries = 3
    e = MaxRetriesReached(retries)
    assert e.retries == retries, f"Expected {retries} but found {e.retries}"
    assert e.cause is None
    assert (
        e.message
        == f"Maximum number of retries ({retries}) has been exceeded. Cause: no further information."
    )


def test_MaxRetriesReached_with_no_retries_and_cause():
    """
    Test for the `MaxRetriesReached` exception with no retries and a cause.

    :return: None
    """
    cause = "test_case: network error"
    e = MaxRetriesReached(cause=cause)
    assert e.retries == "unknown number"
    assert e.cause == cause, f"Expected {cause} but found {e.cause}"
    assert (
        e.message
        == f"Maximum number of retries (unknown number) has been exceeded. Cause: {cause}."
    )


def test_MaxRetriesReached_with_no_retries_and_no_cause():
    """
    Tests the behavior of MaxRetriesReached when there are no retries and no cause.

    :return: None
    """
    e = MaxRetriesReached()
    assert e.retries == "unknown number"
    assert e.cause is None
    assert (
        e.message
        == "Maximum number of retries (unknown number) has been exceeded. Cause: no further information."
    )


def test_json_custom_object_error_without_message():
    """Test JsonCustomObjectError exception without a message."""
    with pytest.raises(JsonCustomObjectError) as exc_info:
        raise JsonCustomObjectError()
    assert str(exc_info.value) == "JSON data contains a custom object."


def test_json_custom_object_error_with_message():
    """Test JsonCustomObjectError with a custom message.

    :return: None
    """
    custom_message = "Custom exception message."
    with pytest.raises(JsonCustomObjectError) as exc_info:
        raise JsonCustomObjectError(custom_message)
    assert str(exc_info.value) == custom_message


def test_json_date_error_with_default_message():
    """Test JsonDateError with a default error message.

    :return: None
    """
    default_message = "JSON data contains a date/time object."
    try:
        raise JsonDateError()
    except JsonDateError as e:
        assert str(e) == default_message


def test_json_date_error_with_custom_message():
    """Test JsonDateError with a custom error message.

    :return: None
    """
    custom_message = "Custom error message."
    try:
        raise JsonDateError(custom_message)
    except JsonDateError as e:
        assert str(e) == custom_message


def test_json_number_precision_error_default_message():
    """Test JsonNumberPrecisionError default error message.

    :return: None
    """
    with pytest.raises(JsonNumberPrecisionError) as e_info:
        raise JsonNumberPrecisionError()
    assert (
        str(e_info.value)
        == "JSON data contains a number with too high precision for Python's float."
    )


def test_json_number_precision_error_custom_message():
    """Test JsonNumberPrecisionError with a custom message.

    :return: None
    """
    custom_message = "This is a customized error message."
    with pytest.raises(JsonNumberPrecisionError) as e_info:
        raise JsonNumberPrecisionError(custom_message)
    assert str(e_info.value) == custom_message
