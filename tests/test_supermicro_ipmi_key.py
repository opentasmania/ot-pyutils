#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test generating a key for Supermicro IPMI license.
"""
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler"
__license__ = "GNU Affero (AGPL) v3"

from pyutils.supermicro_ipmi_key import generate_license_key


def test_generate_license_key_valid_mac():
    """Test that a valid MAC address generates a license key."""
    mac_address = "00:1A:2B:3C:4D:5E"
    license_key = generate_license_key(mac_address)
    assert isinstance(license_key, str)
    assert len(license_key.replace(" ",
                                   "")) == 24
    assert all(char in "0123456789abcdefABCDEF" for char in
               license_key.replace(" ", ""))


def test_generate_license_key_invalid_mac_format():
    """Test that an invalid MAC address format returns None."""
    mac_address = "INVALID:MAC:ADDRESS"
    license_key = generate_license_key(mac_address)
    assert license_key is None


def test_generate_license_key_empty_mac():
    """Test that an empty MAC address returns None."""
    mac_address = ""
    license_key = generate_license_key(mac_address)
    assert license_key is None


def test_generate_license_key_invalid_characters_in_mac():
    """Test that a MAC address with invalid characters returns None."""
    mac_address = "00:1A:2B:3C:4D:5G"  # 'G' is an invalid hex character
    license_key = generate_license_key(mac_address)
    assert license_key is None


def test_generate_license_key_missing_colons():
    """Test that a valid MAC address without colons generates expected output."""
    mac_address = "001A2B3C4D5E"  # No colons but valid hex format
    license_key = generate_license_key(mac_address)
    assert isinstance(license_key, str)
    assert len(license_key.replace(" ",
                                   "")) == 24
    assert all(char in "0123456789abcdefABCDEF" for char in
               license_key.replace(" ", ""))
