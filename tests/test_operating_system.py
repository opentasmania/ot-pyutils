"""Tests for operating system detections."""

# -*- coding: utf-8 -*-
# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

import os
from unittest.mock import patch

from pytest import mark

from pyutils import operating_system


def test_is_micropython_with_no_implementation_property_on_sys(mocker):
    """Check if the current environment is MicroPython."""
    mocker.patch("sys.implementation", new=None)
    assert (
        operating_system.is_micropython() is False
    ), "Expected False but got True"


def test_is_micropython_with_implementation_property_on_sys_but_name_other_than_micropython(
    mocker,
):
    """Test whether is_micropython method returns False when sys.implementation is not 'micropython'.

    :param mocker: The mocker object for patching the sys.implementation.
    :return: None
    """

    class MockImplementation:
        name = "cpython"

    mocker.patch("sys.implementation", new=MockImplementation())
    assert (
        operating_system.is_micropython() is False
    ), "Expected False but got True"


def test_is_micropython_with_sys_implementation_name_as_micropython(mocker):
    """
    Check if the current Python implementation is MicroPython.

    :param mocker: The `mocker` object used for patching `sys.implementation`.
    :return: None

    Example Usage:
    ```
    mocker = Mock()
    test_is_micropython_with_sys_implementation_name_as_micropython(mocker)
    ```
    """

    class MockImplementation:
        name = "micropython"

    mocker.patch("sys.implementation", new=MockImplementation())
    assert (
        operating_system.is_micropython() is True
    ), "Expected True but got False"


def test_get_platform_details():
    """
    Test method for getting platform details.

    :return: None
    """
    with patch("platform.system", return_value="Linux"), patch(
        "platform.version", return_value="5.4.0-74-generic"
    ), patch("platform.architecture", return_value=("64bit", "ELF")):
        result = operating_system.get_platform_details()
        expected_result = {
            "os_name": "Linux",
            "os_version": "5.4.0-74-generic",
            "architecture": ("64bit", "ELF"),
        }
        assert result == expected_result


@mark.parametrize(
    "name, uname, expected",
    [
        ("posix", "Linux", "linux"),
        ("posix", "Darwin", "macos"),
        ("posix", "FreeBSD", "bsd"),
        ("posix", "SunOS", "posix"),
        ("nt", None, "windows"),
        ("unknown", None, None),
    ],
)
def test_os_type(monkeypatch, name, uname, expected):
    """
    Test for the 'get_os_type' method in the 'operating_system' module.

    :param monkeypatch: Monkeypatch object used to set attribute values.
    :param name: Name of the operating system returned by 'os.name'.
    :param uname: Name of the system returned by 'os.uname.sysname' (if applicable).
    :param expected: Expected result of the 'get_os_type' method.
    :return: None
    """
    monkeypatch.setattr(os, "name", name)
    if uname:
        monkeypatch.setattr(
            os, "uname", lambda: type("osName", (), {"sysname": uname})
        )
    assert operating_system.get_os_type() == expected
