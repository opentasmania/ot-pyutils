# -*- coding: utf-8 -*-
"""Functions to test project handling."""

# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

import os

import pytest

from pyutils.project_handling import find_project_root, get_package_root


@pytest.fixture
def project_dir(tmpdir):
    """
    :param tmpdir: A temporary directory created by pytest fixture.

    :return: The path to the created "sub" directory within the temporary directory.
    """
    p = tmpdir.mkdir("sub")
    p.join("LICENCE").write("content")
    os.chdir(str(p))
    return p


@pytest.fixture
def project_dir_sub_dir(tmpdir):
    """Create a project directory with a subdirectory.

    :param tmpdir: Pytest temporary directory fixture.
    :return: Path to the project directory.
    """
    p = tmpdir.mkdir("sub")
    p.join("LICENCE").write("content")
    sub_p = p.mkdir("sub_dir")
    os.chdir(str(sub_p))
    return p


def test_find_project_root_in_current_dir(project_dir):
    """
    Find project root in current directory.

    :param project_dir: The fixture representing the project root directory.
    :return: None
    """
    assert find_project_root() == str(project_dir)


def test_find_project_root_in_sub_dir(project_dir_sub_dir):
    """Test 'find_project_root' that the returned project root path matches the given project_dir_sub_dir.

    :param project_dir_sub_dir: A fixture representing the sub-directory within the project directory.
    :type project_dir_sub_dir: Any

    :return: None

    :raises AssertionError: If the project root path does not match the project_dir_sub_dir argument.
    """
    assert find_project_root() == str(project_dir_sub_dir)


def test_get_package_root_with_None_should_raise_TypeError():
    """Validate calling `get_package_root` method with None as input raises a TypeError.

    :return: None
    """
    with pytest.raises(TypeError):
        get_package_root(None)


def test_get_package_root_with_non_existing_package_should_return_None():
    """Test the behaviour of the get_package."""
    assert get_package_root("nonexistpackage") is None


def test_get_package_root_with_existing_package_should_return_path():
    """This method tests the get_package_root function when a package with the specified name exists.

    :return: This method does not return anything.
    """
    assert get_package_root("pyutils") is not None
