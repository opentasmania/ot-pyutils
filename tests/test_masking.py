# -*- coding: utf-8 -*-
"""Test masking of sensitive info."""

# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

import logging

from pyutils.masking import mask_sensitive_info

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
)


def test_mask_sensitive_info_username():
    """Test mask_sensitive_info function to ensure sensitive information is masked correctly for the username key.

    :return: None
    """
    config = {"username": "testuser", "email": "testuser@mail.com"}
    result = mask_sensitive_info(config)
    assert result["username"] == "****"
    assert result["email"] == config["email"]


def test_mask_sensitive_info_password():
    """Tests the behaviour."""
    config = {"password": "testpassword", "email": "testuser@mail.com"}
    result = mask_sensitive_info(config)
    assert result["password"] == "****"
    assert result["email"] == config["email"]


def test_mask_sensitive_info_login_url():
    """Test the mask_sensitive_info function for login_url.

    This method tests if the login_url in the config dictionary is properly masked.

    Returns:
        None
    """
    config = {
        "login_url": "http://example.com/login",
        "logout_url": "http://example.com/logout",
    }
    result = mask_sensitive_info(config)
    assert result["login_url"] == "****"
    assert result["logout_url"] == config["logout_url"]


def test_mask_sensitive_info_nested_dict():
    """Test the `mask_sensitive_info` method for nested dictionaries.

    This method tests whether the `mask_sensitive_info` method correctly masks sensitive information in
     nested dictionaries.

    Example usage:
        config = {"login": {"username": "testuser", "password": "testpassword"}}
        result = mask_sensitive_info(config)
        assert result["login"]["username"] == "****"
        assert result["login"]["password"] == "****"

    """
    config = {"login": {"username": "testuser", "password": "testpassword"}}
    result = mask_sensitive_info(config)
    assert result["login"]["username"] == "****"
    assert result["login"]["password"] == "****"


def test_mask_sensitive_info_nested_list():
    """Tests the mask_sensitive_info method for a nested list.

    Returns:
        None

    Example:
        test_mask_sensitive_info_nested_list()
    """
    config = {"token": ["tok1", "tok2", {"username": "testuser"}]}
    result = mask_sensitive_info(config)
    assert result["token"][0] == config["token"][0]
    assert result["token"][1] == config["token"][1]
    assert result["token"][2]["username"] == "****"
