# -*- coding: utf-8 -*-
"""Test archive management utilities."""

# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"


import os
import tempfile
import zipfile
from pathlib import Path

from pyutils.archives import extract_files_to_temp_dir


def test_extract_files_to_temp_dir():
    # Creating a temporary zip file for testing
    with tempfile.TemporaryDirectory() as temp_directory:
        zip_path = Path(temp_directory) / "test_archive.zip"
        with zipfile.ZipFile(zip_path, "w") as zip_file:
            zip_file.writestr("test.txt", "Some text")

        # Extracting the zip file using the function to be tested
        temp_directory_object, temp_path = extract_files_to_temp_dir(zip_path)

        # Checking if the extraction was made into temporary directory
        assert temp_directory_object.name == str(temp_path)
        assert Path(temp_path, "test.txt").exists()

        # Checking the contents of the extracted file
        with open(Path(temp_path, "test.txt"), "r") as file:
            assert file.read() == "Some text"

        # Testing if temp_directory_object gets deleted after use
        temp_directory_object.cleanup()
        assert not os.path.exists(temp_directory_object.name)
