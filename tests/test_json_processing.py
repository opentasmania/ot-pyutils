# -*- coding: utf-8 -*-
"""This module contains code for processing json files."""

# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

from datetime import datetime

import pytest

from pyutils.exceptions import (
    JsonCustomObjectError,
    JsonDateError,
    JsonNumberPrecisionError,
)
from pyutils.json_processing import is_jsonable


def test_process_json_properly():
    """Test to ensure JSON data is processed properly.

    :return: None
    """
    # Positive test case: All data types are allowed and none has more than 15 decimal precision
    data = '{"name":"Test", "age":30, "height":1.73, "married":false}'
    is_jsonable(data)


def test_process_json_custom_object_error():
    """Negative test case: Contains disallowed data type CustomObject.

    :return: None
    """

    class CustomObject:
        pass

    # Create an instance of the CustomObject
    custom_object_instance = CustomObject()

    with pytest.raises(JsonCustomObjectError):
        # This should raise a JsonCustomObjectError now
        is_jsonable(custom_object_instance)


def test_process_json_date_error():
    """Test case for processing JSON data with disallowed data type datetime.

    :return:
    """
    data = {
        "birth": datetime.now(),
    }

    with pytest.raises(JsonDateError):
        is_jsonable(data)


def test_process_json_number_precision_error():
    """Test to ensure JsonNumberPrecisionError is raised serializing a number with excessive precision.

    :return: None
    """
    data = {"height": 1.1234567890123456}

    with pytest.raises(JsonNumberPrecisionError):
        is_jsonable(data)
