# -*- coding: utf-8 -*-
"""
    Tests for utilities for dealing with web content.
"""
from pathlib import Path
from typing import List

import pytest

from pyutils.web import (
    _get_assets_from_page,
    _save_page_content,
    ensure_url_scheme,
    mirror_site,
)


@pytest.fixture
def generate_chunks() -> List[bytes]:
    """
    Provides a pytest fixture that generates a list of byte chunks.

    Returns:
        List[bytes]: A list containing two byte strings: "Hello, " and "world!"
    """
    return [b"Hello, ", b"world!"]


def test_save_page_content_save_bytes_content(generate_chunks):
    """
    Tests the `save_page_content` function to ensure it correctly saves byte content to a file.

    Args:
        generate_chunks (Callable): A function that generates chunks of byte content to be saved.

    Raises:
        AssertionError: If the file does not exist at the target path after invoking _save_page_content.
    """
    chunks = generate_chunks
    target_path = "/tmp/index.html"
    _save_page_content(chunks, target_path)
    assert Path(
        target_path
    ).is_file(), "File doesn't exist after _save_page_content execution."


def test_save_page_content_save_inside_directory(generate_chunks):
    """
    Executes a test to verify that the _save_page_content function correctly saves content within a specified directory.

    Args:
        generate_chunks (callable): A function or callable that returns content chunks to be saved.

    Assertions:
        Verifies that an 'index.html' file is created inside the specified directory after calling _save_page_content.
    """
    chunks = generate_chunks
    directory_path = "/tmp/"
    _save_page_content(chunks, directory_path)
    target_path = Path(directory_path, "index.html")
    assert Path(
        target_path
    ).is_file(), "File doesn't exist inside specified directory after _save_page_content execution."


def test_save_page_content_raise_type_error_for_non_bytes(tmp_path):
    """
    Raises a TypeError when non-bytes content is provided to _save_page_content.

    This test verifies that the function _save_page_content raises a TypeError
    when the provided content is not of bytes type. It attempts to save a list
    of strings, which should trigger the exception.
    """
    non_bytes_content = ["Hello, ", "world!"]

    with pytest.raises(TypeError):
        _save_page_content(non_bytes_content, tmp_path / "download")

    # To use non_bytes_content_bytes, you could have an alternate test.
    non_bytes_content_bytes = [s.encode("utf-8") for s in non_bytes_content]
    try:
        _save_page_content(non_bytes_content_bytes, tmp_path / "download")
    except TypeError:
        pytest.fail("save_page_content raised TypeError unexpectedly")


def test_save_page_content_raise_value_error_for_invalid_path(
    generate_chunks,
):
    """
    Test that the 'save_page_content' function raises a ValueError when provided with an invalid file path.

    Parameters:
        generate_chunks: Fixture that generates chunks of web page content.

    Test Steps:
        1. Call 'generate_chunks' to get chunks of web page content.
        2. Define an invalid file path containing a null character.
        3. Use 'pytest.raises' to assert that '_save_page_content' raises a ValueError with the invalid path.
    """
    chunks = generate_chunks
    invalid_path = "\0"
    with pytest.raises(ValueError):
        _save_page_content(chunks, invalid_path)


def test_get_assets_from_page_empty_content():
    """

    Test function to verify the behavior of `get_assets_from_page` when provided with empty content.
    Asserts that no assets are retrieved from an empty webpage content.

        def test_get_assets_from_page_empty_content():
            assert not  _get_assets_from_page("", "http://test.com", "http://test.com")
    """
    assert not _get_assets_from_page(
        html_content="",
        page_url="http://test.com",
        root_url="http://test.com",
    )


def test_get_assets_from_page_valid_content():
    """
    Test retrieval of assets from HTML content.

    The function test_get_assets_from_page_valid_content checks if the  _get_assets_from_page function correctly extracts asset URLs from the provided HTML content. The HTML content contains various asset types such as links, images, scripts, and stylesheets.

    html_content:
        A string containing HTML with various asset tags.

    expected_assets:
        A set of fully-qualified URLs for each asset present in the HTML content.

    Assertions:
        Verifies that the  _get_assets_from_page function produces the correct set of asset URLs when given the html_content and base URL.
    """
    html_content = """
        <html>
        <body>
            <a href="/test.html">Test Page</a>
            <img src="/media/test.png"/>
            <script src="/js/test.js"></script>
            <link rel="stylesheet" href="/styles/test.css">
        </body>
        </html>
    """
    expected_assets = {
        "http://test.com/test.html",
        "http://test.com/media/test.png",
        "http://test.com/js/test.js",
        "http://test.com/styles/test.css",
    }
    assert (
        _get_assets_from_page(
            html_content=html_content,
            page_url="http://test.com",
            root_url="http://test.com",
        )
        == expected_assets
    )


def test_get_assets_from_page_different_domain():
    """

    Unit test to verify that `get_assets_from_page` does not retrieve assets
    from domains different from the given root URL.

    test_get_assets_from_page_different_domain:
        - html_content: A sample HTML string containing a link to an external domain.
        - Asserts that no assets are retrieved when the link is from a different domain
          than the specified root URL.
    """
    html_content = """
        <html>
        <body>
            <a href="http://external.com/test.html">Test Page</a>
        </body>
        </html>
    """
    assert not _get_assets_from_page(
        html_content=html_content,
        page_url="http://test.com",
        root_url="http://test.com",
    )


def test_get_assets_from_page_relative_and_absolute_urls():
    """
    test_get_assets_from_page_relative_and_absolute_urls

    Tests the  _get_assets_from_page function with a sample HTML input containing both relative and absolute URL links.
    It asserts whether the function correctly identifies and returns only the URLs that belong to the specified local domain.

    html_content: A string representing a sample HTML content with various types of <a> tags.
    expected_assets: A set of URLs that are expected to be identified as local by the  _get_assets_from_page function.
    The function checks if the result of  _get_assets_from_page with the given html_content and base URL is equal to expected_assets.
    """
    html_content = """
        <html>
        <body>
            <a href="/test.html">Local Page</a>
            <a href="http://test.com/test2.html">Local Absolute URL</a>
            <a href="http://external.com/test.html">External Page</a>
        </body>
        </html>
    """
    expected_assets = {
        "http://test.com/test.html",
        "http://test.com/test2.html",
    }
    assert (
        _get_assets_from_page(
            html_content=html_content,
            page_url="http://test.com",
            root_url="http://test.com",
        )
        == expected_assets
    )


def test_mirror_site_valid_parameters():
    """
    Unit test for the mirror_site function to verify its behavior with valid parameters.

    Parameters:
    - url: The base URL to start mirroring from.
    - mirror_directory: The directory where the mirrored content will be stored.
    - schemes: List of URL schemes to be considered for mirroring.
    - retries: The number of retries for failed requests.
    - backoff_factor: The factor to control delay between retries.
    - origin: The origin to be mirrored (if different from the provided URL).
    - max_depth: Maximum depth to follow links from the base URL.
    - restrict_to_base_host: Boolean indicating whether to restrict mirroring to the base host.
    - assets_to_ignore: List of asset patterns to ignore during mirroring.
    - exit_on_http_error: Boolean indicating whether to exit the mirroring process on receiving an HTTP error.
    """
    url = "https://example.com"
    mirror_directory = "./mirror"
    schemes = ["http", "https"]
    retries = 5
    backoff_factor = 0.3
    origin = None
    max_depth = 10
    restrict_to_base_host = True
    assets_to_ignore = None
    exit_on_http_error = False

    try:
        mirror_site(
            url,
            mirror_directory,
            schemes,
            retries,
            backoff_factor,
            origin,
            max_depth,
            restrict_to_base_host,
            assets_to_ignore,
            exit_on_http_error,
        )
        assert True
    except Exception as e:
        pytest.fail(f"Test failed with exception: {e}")


def test_mirror_site_negative_max_depth():
    """
    Tests the mirror_site function to ensure it raises a ValueError
    when provided with a negative max_depth parameter.

    This function sets up a context in which mirror_site is called
    with a negative max_depth value. The expected behavior is that
    mirror_site raises a ValueError.

    Parameters:
        None

    Raises:
        ValueError: If max_depth is negative.
    """
    url = "https://example.com"
    mirror_directory = "./mirror"
    schemes = ["http", "https"]

    with pytest.raises(ValueError):
        mirror_site(url, mirror_directory, schemes, max_depth=-5)


def test_mirror_site_schemes_are_validated():
    """

    Tests the mirror_site function to ensure that invalid URL schemes are validated correctly.

    Parameters:
        None

    Valid Cases:
        The function will run with a given URL, mirror directory, and a list of invalid schemes ("ftp", "gopher").

    Expected Result:
        Validation logic in the mirror_site function should identify and handle invalid schemes correctly.

    Returns:
        None

    Assertion:
        Always asserts True to signify that function execution is complete.
    """
    url = "https://example.com"
    mirror_directory = "./mirror"
    schemes = ["ftp", "gopher"]

    mirror_site(url, mirror_directory, schemes)
    assert True


def test_mirror_site_retries_work():
    """
    Test the mirror_site function to check if retry logic works correctly when fetching data from a website.

    This test:
    1. Defines the URL to a flakey website.
    2. Specifies a directory to store the mirrored site content.
    3. Lists the schemes to use for the fetching process, i.e., 'http' and 'https'.
    4. Sets the number of retries to be attempted on failure.

    If the mirror_site function successfully completes within the given retries, the test will pass. If it raises an exception, the test will fail, and the exception details will be provided in the error message.
    """
    url = "https://flakeywebsite.com"
    mirror_directory = "./mirror"
    schemes = ["http", "https"]
    retries = 3

    try:
        mirror_site(url, mirror_directory, schemes, retries=retries)
        assert True
    except Exception as e:
        pytest.fail(f"Test failed with exception: {e}")


def test_ensure_url_scheme():
    """

    test_ensure_url_scheme()
        Tests the ensure_url_scheme function with various input cases.

        - URL without scheme and default scheme provided:
            Verifies the function correctly prepends the default scheme to the URL.

        - URL with scheme and scheme is allowed:
            Confirms that the function returns the URL as-is when the scheme is in the allowed list.

        - URL with scheme and scheme is not allowed:
            Checks that the function raises a ValueError when the URL scheme is not in the allowed list.

        - URL without scheme and disallowed scheme:
            Tests that the function uses the first provided allowed scheme for the URL.

        - Empty URL:
            Ensures the function raises a ValueError with the correct message when provided with an empty URL.
    """
    assert (
        ensure_url_scheme("www.google.com", ["http", "https"])
        == "http://www.google.com"
    )

    assert (
        ensure_url_scheme("http://www.google.com", ["http", "https"])
        == "http://www.google.com"
    )

    with pytest.raises(ValueError):
        ensure_url_scheme("ftp://www.google.com", ["http", "https"])

    assert (
        ensure_url_scheme("www.google.com", ["https"])
        == "https://www.google.com"
    )

    with pytest.raises(ValueError, match="URL cannot be an empty string"):
        ensure_url_scheme("", ["http", "https"])
