# -*- coding: utf-8 -*-
"""Test functions for working with the internet."""

# -*- coding: utf-8 -*-
# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

from pathlib import Path
from unittest.mock import patch, mock_open

import pytest
from pyutils.internet import download_file, is_valid_url
from requests.exceptions import RequestException


def test_is_valid_url():
    # Valid schemes
    assert is_valid_url("http://google.com")
    assert is_valid_url("https://google.com")
    assert is_valid_url("http://www.google.com")
    assert is_valid_url("https://www.google.com")
    assert is_valid_url("ftp://google.com")
    assert is_valid_url("ftps://google.com")
    assert is_valid_url("irc://irc.example.com/channel")
    assert is_valid_url("tel:+1-800-555-5555")
    assert is_valid_url("mailto:someone@example.com")
    assert is_valid_url("ssh://user@host:22")
    assert is_valid_url("git://github.com/user/repo.git")
    assert is_valid_url("svn://svn.example.com/repo")
    assert is_valid_url("sftp://sftp.example.com/file")
    assert is_valid_url("telnet://telnet.example.com")
    assert is_valid_url("ldap://ldap.example.com/ou=people,dc=example,dc=com")
    assert is_valid_url("ws://echo.websocket.org")
    assert is_valid_url("wss://secure.websocket.org")
    assert is_valid_url("data:text/plain;base64,SGVsbG8sIFdvcmxkIQ==")
    assert is_valid_url("file:///home/user/file.txt")
    assert is_valid_url("geo:37.786971,-122.399677")
    assert is_valid_url("magnet:?xt=urn:btih:12345")

    # Invalid URL tests
    assert not is_valid_url("google")
    assert not is_valid_url("www.google.com")
    assert not is_valid_url("")
    assert not is_valid_url(None)
    assert not is_valid_url(12345)
    assert not is_valid_url("//google.com")
    assert not is_valid_url("unsupported://example.com")


@patch("pyutils.internet.get")
@patch("builtins.open", new_callable=mock_open)
def test_download_file_success(mock_file, mock_get):
    mock_get().content = b"test"
    assert download_file("http://test.com", Path("test.txt")) is True
    mock_file.assert_called_once_with("test.txt", "wb")


@patch("pyutils.internet.get")
def test_download_file_request_exception(mock_get):
    mock_get.side_effect = RequestException
    with pytest.raises(RequestException):
        download_file("http://test.com", "test.txt")


@patch("pyutils.internet.get")
@patch("builtins.open", new_callable=mock_open)
def test_download_file_IO_error(mock_file, mock_get):
    mock_get().content = b"test"
    mock_file.side_effect = IOError
    with pytest.raises(IOError):
        download_file("http://test.com", "test.txt")
