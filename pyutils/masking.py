# -*- coding: utf-8 -*-
"""Some routines to mask sensitive information."""

# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@plnom.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@plnom.com>"
__licence__ = "All rights reserved"

from typing import Union


def mask_sensitive_info(config: Union[dict | list]) -> Union[dict | list]:
    """Mask sensitive information in the configuration dictionary.

    :param config: The original configuration dictionary.
    :return: A new dictionary with sensitive information masked.
    """
    sensitive_keys = ["username", "password", "login_url"]

    def mask_dict(d: dict) -> dict:
        """Mask sensitive information in a dictionary by replacing values with '****'.

        Args:
            d (dict): The dictionary to mask sensitive information from.

        Returns:
            dict: The dictionary with the sensitive information masked.

        """
        return {
            k: ("****" if k in sensitive_keys else mask_data(v))
            for k, v in d.items()
        }

    def mask_list(lst: list) -> list:
        """Mask a list.

        Args:
            lst: A list containing sensitive information that needs to be masked.

        Returns:
            A new list with sensitive information masked.

        Raises:
            None

        Example:
            config = {'name': 'John Doe', 'email': 'johndoe@example.com', 'password': 'p@ssw0rd'}
             masked_list = mask_list(config.values())
             print(masked_list)
            ['********', '*********', '*********']
        """
        return [mask_data(item) for item in lst]

    def mask_data(d: Union[dict | list]) -> Union[dict | list]:
        """Mask data.

        Args:
            d: Any data that needs to be masked.

        Returns:
            Any: The masked data.

        """
        if isinstance(d, dict):
            return mask_dict(d)
        elif isinstance(d, list):
            return mask_list(d)
        return d

    return mask_data(config)
