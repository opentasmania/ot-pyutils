# -*- coding: utf-8 -*-
"""A singleton class."""

# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

from typing import Any


class Singleton(type):
    """Singleton Class.

    A metaclass for creating singleton classes.

    Attributes:
        _instances (dict): A dictionary to store the instances of the singleton classes.

    Methods:
        __call__ : Method to create or return an instance of the singleton class.

    Examples:
        class MyClass(metaclass=Singleton):
            pass
    """

    _instances: dict[Any, object] = {}

    def __call__(cls, *args, **kwargs):
        """Invoke a singleton class as a callable.

        When the singleton class is invoked as a callable, this method checks if an instance of the class
        has already been created. If yes, it returns the existing instance; otherwise, it creates a new instance
        of the singleton class using the given arguments and stores it for future use.

        :param cls: The singleton class.
        :param args: Positional arguments passed to the singleton class.
        :param kwargs: Keyword arguments passed to the singleton class.
        :return: The instance of the singleton class.

        Example:
        .. code-block:: python

            class Singleton:
                _instances = {}

                def __call__(cls, *args, **kwargs):
                    if cls not in cls._instances:
                        cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
                    return cls._instances[cls]

            instance1 = Singleton()
            instance2 = Singleton()

            print(instance1 is instance2)  # Output: True
        """
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(
                *args, **kwargs
            )
        return cls._instances[cls]
