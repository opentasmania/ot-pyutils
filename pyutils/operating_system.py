"""Functions for operating system detection."""

# -*- coding: utf-8 -*-
# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

import os
import platform
import sys
from typing import Dict, Union


def is_micropython() -> bool:
    """Check if the current Python implementation is MicroPython.

    :return: True if running on MicroPython, False otherwise.
    :rtype: bool
    """
    if (
        hasattr(sys, "implementation")
        and sys.implementation is not None
        and sys.implementation.name == "micropython"
    ):
        return True
    return False


def get_platform_details() -> Dict[str, Union[str, tuple]]:
    """Retrieve and return details of the operating system.

    :return: A dictionary containing the following details:
        - os_name (str): The name of the operating system.
        - os_version (str): The version of the operating system.
        - architecture (tuple): The CPU architecture.

    Example:
    get_os_details()
    {
        'os_name': 'Windows',
        'os_version': '10.0.18363',
        'architecture': ('64bit', 'WindowsPE')
    }
    """
    # Retrieve and return OS details
    return {
        "os_name": platform.system(),
        "os_version": platform.version(),
        "architecture": platform.architecture(),
    }


def get_os_type():
    """Detect the operating system type.

    Returns:
        - 'linux' for Linux
        - 'macos' for macOS
        - 'bsd' for BSD systems
        - 'posix' for other POSIX-compliant systems
        - 'windows' for Windows
        - None for unsupported systems
    """
    if is_micropython():
        return "micropython"
    elif os.name == "posix":
        sysname = os.uname().sysname
        if sysname == "Linux":
            return "linux"
        elif sysname == "Darwin":
            return "macos"
        elif "bsd" in sysname.lower():
            return "bsd"
        else:
            return "posix"
    elif os.name == "nt":
        return "windows"
    else:
        return None
