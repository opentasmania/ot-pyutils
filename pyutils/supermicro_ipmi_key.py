#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Generate a key for Supermicro IPMI license. To only be used for testing.
A licence must be purchased from Supermicro once system operation has been confirmed.
"""
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler"
__license__ = "GNU Affero (AGPL) v3"

import sys
import hmac
import hashlib
import re
from typing import Optional

IPMI_KEY = "8544E3B47ECA58F9583043F8"
LICENSE_KEY_LENGTH = 24
CHUNK_SIZE = 4


def generate_license_key(mac_address: str) -> Optional[str]:
    """Generates an IPMI license key from a MAC address."""
    if not mac_address:  # Check for empty or None mac_address
        return None
    try:
        mac_bytes = bytes.fromhex(mac_address.replace(":", ""))
    except ValueError:
        return None
    key = bytes.fromhex(IPMI_KEY)
    license_key = hmac.new(key, mac_bytes, hashlib.sha1).hexdigest()[
        :LICENSE_KEY_LENGTH
    ]
    return " ".join(
        license_key[i : i + CHUNK_SIZE]
        for i in range(0, LICENSE_KEY_LENGTH, CHUNK_SIZE)
    )


def main():
    if len(sys.argv) <= 1:
        sys.exit(f"Usage: {sys.argv[0]} <MAC>")
    mac_address = sys.argv[1]
    if not re.match(r"^([0-9A-Fa-f]{2}:){5}[0-9A-Fa-f]{2}$", mac_address):
        sys.exit(f"Invalid MAC address: {mac_address}")
    license_key = generate_license_key(mac_address)
    if license_key:
        formatted_license_key = license_key
        print(f"Licence key: {formatted_license_key}")
    else:
        sys.exit(f"Invalid MAC address format: {mac_address}")


if __name__ == "__main__":
    main()
