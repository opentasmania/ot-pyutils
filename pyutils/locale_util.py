# -*- coding: utf-8 -*-
"""
Locale handling for pyutils.
"""
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler"
__license__ = "GNU Affero (AGPL) v3"

import gettext
import locale
from pathlib import Path

INFO_MESSAGES = {
    "create_directory": "Creating directory: {directory},",
}

VERBOSE_MESSAGES = {
    "renaming": "Renaming: {old_filename} to {new_filename}",
}

ERROR_MESSAGES = {
    "access": "Error accessing file {file} in directory {root}: {e}",
    "rename": "Error renaming {file} to {new_name}: {e}",
    "directory_not_found": "The directory {directory} does not exist or is not a directory.",
    "error_removing_file": "Error removing file {file}: {e}",
    "create_directory": "Cannot create directory {directory}: {e}",
}

DRY_RUN_MESSAGES = {
    "dry_run_rename_file": "[Dry Run] Would rename: {file} to {unique_name}",
    "dry_run_remove_file": "[Dry Run] Would remove duplicate file: {file}",
    "dry_run_create_directory": "[Dry Run] Would create directory: {directory}",
}

ARGUMENT_PARSER_STRINGS = {
    "directory_help": "Directory to search in (default: current directory).",
    "verbosity_help": "Increase output verbosity.",
}

IMPORT_FUNCTION_FROM_MODULE_ERROR_MESSAGE = {
    "import_error": "Warning: Could not import {function} from {module}. "
    "Ensure {module} is fully in the source location. ",
    "functionality_unavailable_past_tense": "{functionality} was not be available.",
    "functionality_unavailable_present_tense": "{functionality} is not be available.",
    "functionality_unavailable_future_tense": "{functionality} will not be available.",
}

FILENAME_FRIENDLY_DATE_FORMAT = "%Y-%m-%d-%H-%M-%S"


def configure_locale(domain: str, localedir: Path):
    """
    Configures and sets the locale and text domain for internationalization.

    Args:
        domain (str): The domain under which the .mo files are stored.
        localedir (Path): The directory containing the locale data.

    Returns:
        function: The translation function __() for the specified domain.
    """
    locale.setlocale(locale.LC_ALL, "")
    gettext.bindtextdomain(domain, localedir)
    gettext.textdomain(domain)
    return gettext.gettext
