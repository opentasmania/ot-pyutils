# -*- coding: utf-8 -*-
"""
    Utilities for dealing with web content.
"""
# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@plnom.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@plnom.com>"
__licence__ = "All rights reserved"

from collections import deque
from logging import (
    INFO,
    basicConfig,
    getLogger,
    debug,
    error,
    info,
    warning,
    StreamHandler,
)
from os.path import dirname, exists, join
from pathlib import Path
from time import sleep
from typing import Dict, Iterable, List, Optional, Set
from urllib.parse import urlparse, urljoin, urlunparse

from bs4 import BeautifulSoup
from requests import Session
from requests.adapters import HTTPAdapter
from requests.exceptions import HTTPError
from requests.models import Response
from urllib3.util.retry import Retry

VALID_SCHEMES = {
    "http",
    "https",
    "ftp",
    "ftps",
    "mailto",
    "file",
    "data",
    "irc",
    "ssh",
}

logger = getLogger(__name__)
if not logger.hasHandlers():
    basicConfig(
        level=INFO,
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        handlers=[StreamHandler()],
    )


def _save_page_content(content: Iterable[bytes], download_path: str) -> None:
    """
    Saves the content to a specified file path.
    Parameters:
        content (Iterable[bytes]): An iterable containing content chunks in bytes.
        download_path (str): The path where the content will be saved.
    Raises:
        TypeError: If any item in content is not of type bytes.
        ValueError: If content is empty.
    Behavior:
        If download_path is a directory, content will be saved to `index.html` in that directory.
        Opens the file at download_path in write-binary mode and writes each chunk from content.
    """
    if not content:
        raise ValueError("Content cannot be empty")
    if not all(isinstance(chunk, bytes) for chunk in content):
        raise TypeError("All items in content must be bytes")
    if Path(download_path).is_dir():
        download_path = join(download_path, "index.html")
        debug(f"Directory detected. Saving content to {download_path}")
    debug(f"Saving content to {download_path}")
    download_dir = Path(download_path).parent
    if not download_dir.exists():
        debug(f"Creating directory {download_dir} since it does not exist.")
        download_dir.mkdir(parents=True, exist_ok=True)
    try:
        with open(download_path, "wb") as f:
            for chunk in content:
                f.write(chunk)
    except (IOError, OSError) as e:
        error(f"Error writing to {download_path}: {str(e)}")
        raise


def _get_assets_from_page(
    html_content: str, page_url: str, root_url: str
) -> Set[str]:
    """
    Extracts and returns a set of asset URLs from the given HTML content.
    Parameters:
        html_content (str): The HTML content of the page.
        page_url (str): The base URL of the page, used to resolve relative URLs.
        root_url (str): The URL of the mirror site, used to restrict asset extraction to the same domain.
    Returns:
        set: A set containing the full URLs of assets (hyperlinks, images, JavaScript files, and CSS files) found in the HTML content.
    """

    def normalize_url(url: str) -> str:
        """
        Extracts and normalizes asset URLs from a given HTML page content.

        :param html_content: The HTML content of the page as a string.
        :param page_url: The URL of the current page where the content was fetched.
        :param root_url: The root URL of the website.

        :returns: A set of normalized asset URLs found in the HTML content.
        """
        parsed_url = urlparse(url)
        return str(urlunparse(parsed_url._replace(fragment="", query="")))

    try:
        soup = BeautifulSoup(html_content, "html.parser")
    except Exception as e:
        error(f"Error parsing HTML content: {str(e)}")
        return set()

    asset_links = set()

    # Define tag-attribute pairs to search for assets
    tag_attr_pairs = [
        ("a", "href"),
        ("img", "src"),
        ("script", "src"),
        ("link", "href"),
        ("video", "src"),
        ("audio", "src"),
    ]

    for tag, attr in tag_attr_pairs:
        for element in soup.find_all(tag):
            url = element.get(attr)
            if url:
                full_url = normalize_url(urljoin(page_url, url))
                parsed_full_url = urlparse(full_url)
                parsed_base_url = urlparse(root_url)
                if parsed_full_url.netloc == parsed_base_url.netloc:
                    debug(f"Asset link addition: {full_url}")
                    asset_links.add(full_url)

    return asset_links


def _adjust_links_to_local(
    html_content: str, page_url: str, mirror_directory: str, hostname: str
) -> str:
    """
    Adjusts the links within the given HTML content to point to the local mirror directory.

    Parameters:
    - html_content: The HTML content as a string.
    - page_url: The URL of the page from which the HTML content was fetched.
    - mirror_directory: The local directory where the mirrored content is stored.
    - hostname: The hostname of the website being mirrored.

    Returns:
    - The modified HTML content as a string with adjusted links to the local mirror directory.
    """
    soup = BeautifulSoup(html_content, "html.parser")
    tag_attr_pairs = [
        ("a", "href"),
        ("img", "src"),
        ("script", "src"),
        ("link", "href"),
        ("video", "src"),
        ("audio", "src"),
    ]

    for tag, attr in tag_attr_pairs:
        for element in soup.find_all(tag):
            url = element.get(attr)
            if url:
                full_url = urljoin(page_url, url)
                parsed_url = urlparse(full_url)
                if parsed_url.netloc != hostname:
                    continue

                relative_path = parsed_url.path.lstrip("/")
                local_path = join(mirror_directory, hostname, relative_path)
                relative_local_path = str(
                    Path(local_path).relative_to(mirror_directory)
                )
                element[attr] = relative_local_path
    return str(soup)


def ensure_url_scheme(url: str, allowed_schemes: list[str]) -> str:
    """
    Ensures that the URL has a scheme from the allowed_schemes list.
    If not, it raises a ValueError.

    :param url: The URL to check.
    :param allowed_schemes: A list of allowed URL schemes.
    :return: The URL with a valid scheme.
    :raises ValueError: If the URL does not have a valid scheme.
    """
    if not url:
        raise ValueError("URL cannot be an empty string")

    if not all(scheme in VALID_SCHEMES for scheme in allowed_schemes):
        invalid_schemes = [
            scheme
            for scheme in allowed_schemes
            if scheme not in VALID_SCHEMES
        ]
        raise ValueError(
            f"Invalid URL schemes in allowed_schemes: {invalid_schemes}"
        )

    parsed_url = urlparse(url)

    if not parsed_url.scheme:
        scheme = allowed_schemes[0]
        netloc_with_path = f"{parsed_url.netloc}{parsed_url.path}"
        return urlunparse((scheme, netloc_with_path, "", "", "", ""))

    if parsed_url.scheme not in allowed_schemes:
        raise ValueError(
            f"URL scheme '{parsed_url.scheme}' is not in the list of allowed schemes: {allowed_schemes}"
        )

    return url


def mirror_site(
    url: str,
    mirror_directory: str,
    schemes: List[str],
    retries: int = 5,
    backoff_factor: float = 0.3,
    origin: Optional[str] = None,
    max_depth: int = 10,
    restrict_to_base_host: bool = True,
    assets_to_ignore: Optional[List[str]] = None,
    exit_on_http_error: bool = False,
) -> None:
    """
    Retrieves and stores a mirrored copy of a website up to a specified depth.
    Args:
        url (str): The base URL of the site to mirror.
        mirror_directory (str): Local directory where the mirrored website will be stored.
        schemes (List[str]): List of allowed URL schemes (e.g., ["http", "https"]).
        retries (int, optional): Number of times to retry failed requests. Defaults to 5.
        backoff_factor (float, optional): A backoff factor to apply between attempts. Defaults to 0.3.
        origin (Optional[str], optional): Origin information for logging purposes. Defaults to None.
        max_depth (int, optional): Maximum depth to search for links. Defaults to 10.
        restrict_to_base_host (bool, optional): Whether to restrict mirroring to the base host. Defaults to True.
        assets_to_ignore (Optional[List[str]], optional): List of asset URLs to ignore. Defaults to None.
        exit_on_http_error (bool, optional): Whether to stop execution on HTTP errors. Defaults to False.
    Raises:
        ValueError: If 'max_depth' is a negative integer, or if a URL's hostname is None.
        HTTPError: If a critical HTTP error occurs and 'exit_on_http_error' is set to True.
    """
    if max_depth < 0:
        raise ValueError("Max depth should be a non-negative integer")
    mirror_directory = mirror_directory.rstrip("/")
    session = Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=[429, 500, 502, 503, 504],
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount("http://", adapter)
    session.mount("https://", adapter)
    parsed_url = urlparse(url)
    base_url = f"{parsed_url.scheme}://{parsed_url.netloc}"
    urls_to_visit = deque([(url, 0)])
    visited: Set[str] = set()
    four_oh_four_not_found_errors: List[Dict[str, Optional[str]]] = []
    while urls_to_visit:
        response: Optional[Response] = None
        current_url, current_depth = urls_to_visit.popleft()
        if current_url in visited or current_depth > max_depth:
            continue
        visited.add(current_url)
        info(f"Retrieving: {current_url}")
        try:
            response = session.get(current_url, stream=True)
            response.raise_for_status()
        except HTTPError as http_err:
            if (response is not None) and (response.status_code == 404):
                warning(f"404 Error: Origin: {origin}, URL: {current_url}")
                four_oh_four_not_found_errors.append(
                    {"url": current_url, "origin": origin}
                )
            else:
                error(f"HTTP Error encountered: {str(http_err)}")
                if exit_on_http_error:
                    raise http_err
            continue
        except Exception as http_err:
            error(f"General Error encountered: {str(http_err)}")
            if exit_on_http_error:
                raise http_err
        if (response is not None) and (response.status_code == 200):
            content = response.content
            current_parsed_url = urlparse(current_url)
            relative_path = current_parsed_url.path.lstrip("/")
            if current_parsed_url.hostname is None:
                raise ValueError(
                    f"{current_parsed_url} hostname cannot be None"
                )
            local_path = join(
                mirror_directory, current_parsed_url.hostname, relative_path
            )
            # Check Content-Disposition header for suggested filename
            content_disposition = response.headers.get("Content-Disposition")
            debug(f"Content-Disposition: {content_disposition}")
            if content_disposition:
                filename = ""
                # Extract filename from Content-Disposition header
                if "filename=" in content_disposition:
                    filename = content_disposition.split("filename=")[-1].strip('"')
                # If a filename is suggested, use it
                if filename:
                    local_path = join(local_path, filename)
            if not exists(dirname(local_path)):
                Path(dirname(local_path)).mkdir(parents=True, exist_ok=True)
            if local_path.endswith("/"):
                local_path = join(local_path, "index.html")
            encoding = response.apparent_encoding or "utf-8"
            try:
                decoded_content = content.decode(encoding)
            except UnicodeDecodeError:
                decoded_content = content.decode("latin1")

            adjusted_content = _adjust_links_to_local(
                decoded_content,
                current_url,
                mirror_directory,
                current_parsed_url.hostname,
            )

            info(f"Saving: {current_url} to {local_path}")
            _save_page_content([adjusted_content.encode("utf-8")], local_path)

            asset_links = _get_assets_from_page(
                decoded_content, current_url, base_url
            )
            for asset_url in asset_links:
                parsed_asset_url = urlparse(asset_url)
                if parsed_asset_url.scheme == "mailto":
                    warning(f"Skipping mailto URL: {asset_url}")
                    continue
                try:
                    ensure_url_scheme(url=asset_url, allowed_schemes=schemes)
                except ValueError as ve:
                    warning(f"Skipping invalid URL: {asset_url}. Error: {ve}")
                    continue
                if (
                    restrict_to_base_host
                    and parsed_asset_url.netloc != parsed_url.netloc
                ):
                    debug(
                        f"Skipping external URL due to restriction: {asset_url}"
                    )
                    continue
                if assets_to_ignore and asset_url in assets_to_ignore:
                    debug(f"Skipping URL due to ignore list: {asset_url}")
                    continue
                if (asset_url, current_depth + 1) not in urls_to_visit:
                    urls_to_visit.append((asset_url, current_depth + 1))
        elif (response is not None) and (response.status_code == 429):
            warning("Got 429 too many requests, sleeping for 60 seconds.")
            sleep(60)
            urls_to_visit.appendleft((current_url, current_depth))
        debug(f"Total 404s encountered: {len(four_oh_four_not_found_errors)}")
        for four_oh_four_error in four_oh_four_not_found_errors:
            warning(
                f"404 URL: {four_oh_four_error['url']} Origin: {four_oh_four_error['origin']}"
            )
