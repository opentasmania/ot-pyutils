# -*- coding: utf-8 -*-
"""
    Routines for hashes
"""

import hashlib


class HashProcessor:
    """
    class HashProcessor:

    def __init__(self, algorithm='sha256'):
    """

    def __init__(self, algorithm="sha256"):
        self.algorithm = algorithm
        self._hash_object = hashlib.new(algorithm)

    def update(self, data):
        """

        Updates the hash object with the provided data.

        Args:
            data (str): The data to update the hash object with. The data will be encoded to 'utf-8' before updating the hash object.

        """
        self._hash_object.update(data.encode("utf-8"))

    def hexdigest(self):
        """
        Computes the hexadecimal representation of the hash object.

        Returns:
            str: The hexadecimal digest of the hash object.
        """
        return self._hash_object.hexdigest()

    def reset(self):
        """
        Resets the internal hash object to its initial state.

        This method reinitializes the hash object with the specified algorithm,
        effectively clearing any previously accumulated data and starting the
        hash computation from scratch.
        """
        self._hash_object = hashlib.new(self.algorithm)

    def compare(self, data, hashed_value):
        """

        Compares a given data string with a hashed value.

        Args:
         data (str): The input string to be hashed and compared.
         hashed_value (str): The hashed value to compare against.

        Returns:
         bool: True if the hashed input string matches the hashed_value, False otherwise.
        """
        self.update(data)
        return self.hexdigest() == hashed_value
