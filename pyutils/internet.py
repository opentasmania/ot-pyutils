# -*- coding: utf-8 -*-
"""Functions for working with the internet."""

# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

from logging import getLogger, critical, info
from typing import Union
from urllib.parse import urlparse
from pathlib import Path

from requests import get
from requests.exceptions import RequestException

logger = getLogger(__name__)


def is_valid_url(url):
    """
    Checks if a given URL is valid.

    The function uses urlparse to parse the input URL and checks for the presence of a scheme and a netloc or path, depending on the scheme.
    It handles exceptions like ValueError, AttributeError, and TypeError and returns False in such cases.

    Parameters:
        url (str): The URL string to be validated.

    Returns:
        bool: True if the URL is valid, False otherwise.
    """
    accepted_schemes = [
        "http",
        "https",
        "ftp",
        "ftps",
        "irc",
        "tel",
        "mailto",
        "ssh",
        "git",
        "svn",
        "sftp",
        "telnet",
        "ldap",
        "ws",
        "wss",
        "data",
        "file",
        "geo",
        "magnet",
    ]
    try:
        result = urlparse(url)
        if result.scheme in accepted_schemes:
            # Schemes like tel, mailto, file, data, geo, and magnet can have missing netloc but must have a valid path or query
            if result.scheme in [
                "tel",
                "mailto",
                "file",
                "data",
                "geo",
                "magnet",
            ]:
                return bool(result.path or result.query)
            # Other schemes like HTTP/HTTPS, FTP, WS/WSS require both scheme and netloc
            return all([result.scheme, result.netloc])
        return False
    except (ValueError, AttributeError, TypeError):
        return False


def download_file(url: str, file_path: Union[str, Path]) -> Union[bool, None]:
    """
    Downloads a file from a given URL and saves it to the specified file path.
    Parameters:
    url (str): The URL of the file to be downloaded.
    file_path (Union[str, Path]): The destination path where the file will be saved.
    Raises:
    Exception: If there is an error during the download process.
    Logs:
    info: If the file is successfully downloaded.
    critical: If the file cannot be downloaded due to a non-200 response.
    """
    file_path = Path(file_path)

    try:
        response = get(url)
        response.raise_for_status()
        try:
            with open(str(file_path), "wb") as f:
                f.write(response.content)
                info(f"Downloaded data to {file_path}")
                return True
        except FileNotFoundError as e:
            critical(f"FileNotFoundError: {e}")
            raise
    except RequestException as e:
        critical(f"RequestException: {e}")
        raise
