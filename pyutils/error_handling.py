"""Functions for handling errors."""

# -*- coding: utf-8 -*-
# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

from logging import getLogger, debug
from time import sleep
from typing import Callable, Optional, Tuple, Type

from .exceptions import MaxRetriesReached

logger = getLogger(__name__)


def with_retry(
    retryable_operation: Callable,
    exception_types: Optional[Tuple[Type[Exception]]] = None,
    max_attempts: int = 3,
):
    """Retry a given operation when specified exceptions occur.

    Operate and catch any exceptions specified in `exception_types` tuple.
    If an exception is caught, wait for a specific duration defined by `initial_wait`, and then retry the operation.
    The process continues for a maximum of `max_attempts` times.
    If the last attempt also fails, an exception is raised with a message.
    The message indicates the number of attempts made, and the specific exception that caused the failure.

    The wait time between each retry attempt is increased exponentially by multiplying the wait time by `2 * attempt`.

    :param retryable_operation: A function or method that represents the operation to be retried.
    :param exception_types: A tuple of exception types that will be caught during the retry process.
    :param max_attempts: An integer representing the maximum number of attempts to execute the operation. Default is 3.


    Example usage:
    ```python
    def my_operation():
        # code for the operation

    result = with_retry(my_operation, (MyException,), max_attempts=5)
    ```
    """
    exception_types = exception_types if exception_types else (Exception,)
    debug(f"Max attempts: {max_attempts}")
    for attempt in range(1, max_attempts + 1):
        try:
            debug(f"Attempt: {attempt}")
            return retryable_operation()
        except exception_types as e:
            debug(
                f"Attempt {attempt} failed due to {type(e).__name__}. Retrying..."
            )
            sleep(attempt)
            if attempt == max_attempts:
                raise MaxRetriesReached(max_attempts, e) from e
