# -*- coding: utf-8 -*-
"""Archive management utilities."""

# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

from logging import getLogger, debug
from pathlib import Path
from tempfile import TemporaryDirectory

from patoolib import extract_archive  # type: ignore


logger = getLogger(__name__)


def extract_files_to_temp_dir(
    zipfile_full_path: Path,
) -> tuple[TemporaryDirectory[str], Path]:
    """
    Extracts a Zip file to a temporary directory.

    Args:
        zipfile_full_path (Path): The full path to the Zip file to extract.

    Returns:
        tuple: A tuple containing the TemporaryDirectory object and the Path to the temporary directory.

    Side Effects:
        Creates a temporary directory and extracts the contents of the Zip file into it.
        The caller should clean up temp_directory_object when finished with it.
    """
    temp_directory_object = TemporaryDirectory()
    debug(f"created {temp_directory_object}")
    temp_path = Path(temp_directory_object.name)
    debug(f"temp_path = {temp_path}")
    extract_archive(
        archive=str(zipfile_full_path),
        outdir=str(temp_path),
    )
    return temp_directory_object, temp_path
