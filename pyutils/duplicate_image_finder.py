# -*- coding: utf-8 -*-
"""Functions for detecting duplicate images."""
# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

from argparse import ArgumentParser
from os import listdir, path
from typing import Optional, List, Tuple, Dict

from imagehash import ImageHash, phash
from numpy import bitwise_or, zeros
from rawpy import imread  # type: ignore
from tifffile import TiffFile
from PIL import Image, UnidentifiedImageError


def calculate_image_hash(
    file_path: str, mode: str, frame_number: int
) -> Optional[ImageHash]:
    """Calculate the perceptual hash (pHash) for an image at the given file path."""
    try:
        if file_path.endswith(".dng"):
            with imread(file_path) as raw:
                rgb = raw.postprocess()
                img = Image.fromarray(rgb)
        elif file_path.endswith(".tif") or file_path.endswith(".tiff"):
            with TiffFile(file_path) as tif:
                img = Image.fromarray(tif.asarray())
        else:
            img = Image.open(file_path)
            if getattr(
                img, "is_animated", False
            ):  # Check for animated images
                if mode == "single":
                    try:
                        img.seek(frame_number)
                    except EOFError:
                        print(
                            f"Error: Frame {frame_number} does not exist. "
                            f"The image has {getattr(img, 'n_frames', 1)} frames."
                        )
                        return None
                elif mode == "whole":
                    # Calculate a composite hash for the entire sequence
                    hash_accum = ImageHash(
                        zeros(phash(img).hash.shape, dtype=bool)
                    )
                    for frame in range(getattr(img, "n_frames", 1)):
                        img.seek(frame)
                        hash_accum.hash = bitwise_or(
                            hash_accum.hash, phash(img).hash
                        )
                    return hash_accum
        return phash(img)
    except UnidentifiedImageError:
        print(f"Error: {file_path} is an unsupported image format.")
    except Exception as e:
        print(f"Error processing {file_path}: {e}")
    return None


def compare_images(
    hash1: ImageHash,
    hash2: ImageHash,
    file_path1: str,
    file_path2: str,
    image_tol: int,
    metadata_tol: float,
) -> bool:
    """Compare two images by their hash values and metadata if necessary."""
    if hash1 - hash2 <= image_tol:
        if any(
            file_path1.endswith(ext) for ext in [".dng", ".tif", ".tiff"]
        ) and any(
            file_path2.endswith(ext) for ext in [".dng", ".tif", ".tiff"]
        ):
            metadata1 = calculate_metadata(file_path1)
            metadata2 = calculate_metadata(file_path2)
            if metadata1 is None or metadata2 is None:
                return False
            return compare_metadata(metadata1, metadata2, metadata_tol)
        return True
    return False


def calculate_metadata(file_path: str) -> Optional[Dict]:
    """Extract and return metadata from an image file."""
    try:
        if file_path.endswith(".dng"):
            with imread(file_path) as raw:
                return {
                    "color_profile": raw.color_profile,
                    "white_balance": raw.white_balance,
                    "exposure_time": raw.exposure_time,
                }
        elif file_path.endswith(".tif") or file_path.endswith(".tiff"):
            with TiffFile(file_path) as tif:
                metadata = {}
                if tif.pages:
                    page = tif.pages[0]
                    # Check if the page has tags attribute
                    if hasattr(page, "tags"):
                        metadata = {
                            "dpi": page.tags["XResolution"].value,
                            "compression": page.tags["Compression"].value,
                        }
                return metadata
        else:
            img = Image.open(file_path)
            try:
                return {
                    "resolution": img.size,
                    "creation_time": path.getctime(file_path),
                }
            finally:
                img.close()
    except (KeyError, OSError) as e:
        print(f"Error processing metadata for {file_path}: {e}")
        return None


def compare_metadata(metadata1: Dict, metadata2: Dict, tol: float) -> bool:
    """Compare two sets of metadata to check if they are within a specified tolerance."""
    for field in ["exposure_time", "white_balance"]:
        if field in metadata1 and field in metadata2:
            if abs(metadata1[field] - metadata2[field]) > tol:
                return False
    return metadata1.get("color_profile") == metadata2.get("color_profile")


def find_duplicates(
    directory: str,
    image_tol: int,
    metadata_tol: float,
    mode: str,
    frame_number: int,
) -> List[Tuple[str, str]]:
    """Find duplicate images in a given directory based on image hash values."""
    hash_to_file_paths: Dict[ImageHash, List[str]] = {}
    duplicates = []
    for filename in listdir(directory):
        file_path = path.join(directory, filename)
        hash_val = calculate_image_hash(file_path, mode, frame_number)
        if hash_val:
            if hash_val in hash_to_file_paths:
                for existing_file_path in hash_to_file_paths[hash_val]:
                    if compare_images(
                        hash_val,
                        hash_val,
                        file_path,
                        existing_file_path,
                        image_tol=image_tol,
                        metadata_tol=metadata_tol,
                    ):
                        duplicates.append((existing_file_path, file_path))
                hash_to_file_paths[hash_val].append(file_path)
            else:
                hash_to_file_paths[hash_val] = [file_path]
    return duplicates


if __name__ == "__main__":
    parser = ArgumentParser(
        description="Find duplicate images in a directory"
    )
    parser.add_argument("directory", help="The directory to search")
    parser.add_argument(
        "-it",
        "--image_tol",
        type=int,
        default=5,
        help="Tolerance for image hash comparison",
    )
    parser.add_argument(
        "-mt",
        "--metadata_tol",
        type=float,
        default=0.05,
        help="Tolerance for metadata comparison",
    )
    parser.add_argument(
        "-am",
        "--animated-mode",
        choices=["single", "whole"],
        default="single",
        help="Mode for handling animated images",
    )
    parser.add_argument(
        "-f",
        "--frame",
        type=int,
        default=0,
        help="Frame number to analyze for single frame mode (default is 0)",
    )
    args = parser.parse_args()
    duplicates = find_duplicates(
        args.directory,
        args.image_tol,
        args.metadata_tol,
        args.mode,
        args.frame,
    )
    if duplicates:
        print("Duplicate images found:")
        for duplicate_pair in duplicates:
            print(duplicate_pair)
    else:
        print("No duplicate images found.")
