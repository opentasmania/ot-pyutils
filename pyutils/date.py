# -*- coding: utf-8 -*-
"""This module contains code for date handling."""

# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

from datetime import datetime, timezone
from logging import getLogger
from re import search
from pytz import utc

logger = getLogger(__name__)


def is_date(string: str) -> bool:
    """Check that a given string is a valid date format in "%Y-%m-%dT%H:%M:%S".

    If the string is a valid date, it returns True; otherwise, it returns False.

    :param string: a string representing a date in the format "%Y-%m-%dT%H:%M:%S"
    :return: True if the string is a valid date, False otherwise
    """
    try:
        datetime.strptime(string, "%Y-%m-%dT%H:%M:%S")
        return True
    except ValueError:
        return False


def epoch_datetime() -> datetime:
    """
    Returns the datetime object for the Unix epoch (January 1, 1970, 00:00:00 UTC).

    Returns:
        datetime: The datetime object representing the start of the Unix epoch in UTC.
    """
    return datetime(
        year=1970,
        month=1,
        day=1,
        hour=0,
        minute=0,
        second=0,
        microsecond=0,
        tzinfo=timezone.utc,
    )


def format_datetime(dt: datetime, fmt: str) -> str:
    """
    Formats a datetime object into a string representation based on the provided format.

    :param dt: The datetime object to format.
    :param fmt: The format string to use for formatting the datetime object.
    :return: A formatted string representation of the datetime object.
    """
    if dt is None:
        raise TypeError("The datetime object cannot be None")
    if fmt is None:
        raise TypeError("The format string cannot be None")
    if not isinstance(dt, datetime):
        raise TypeError("The input must be a datetime object")

    # Ensure the format string contains at least one valid datetime specifier
    if not search(r"%[a-zA-Z]", fmt):
        raise ValueError("Invalid format string")

    try:
        return dt.strftime(fmt)
    except ValueError as e:
        raise ValueError("Invalid format string") from e


def localize_datetime(dt: datetime) -> datetime:
    """
    Converts a naive datetime object to an aware datetime object using UTC as the time zone.
    If the datetime object is already aware, it returns the datetime object unchanged.

    Args:
        dt (datetime): The datetime object to be localized. It can be either naive or aware.

    Returns:
        datetime: A timezone-aware datetime object in UTC.
    """
    return utc.localize(dt) if dt.tzinfo is None else dt
