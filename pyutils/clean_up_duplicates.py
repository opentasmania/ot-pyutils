#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Find duplicate files by regex in a directory and rename based on timestamp.
"""
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler"
__license__ = "GNU Affero (AGPL) v3"

import sys
from argparse import ArgumentParser, RawTextHelpFormatter
from logging import basicConfig, DEBUG, ERROR, getLogger, INFO
from pathlib import Path
from typing import Any, Optional

script_dir = Path(__file__).parent.parent.resolve()
sys.path.insert(0, str(script_dir))

try:
    from pyutils.locale_util import (
        configure_locale,
        VERBOSE_MESSAGES,
        DRY_RUN_MESSAGES,
        ERROR_MESSAGES,
        IMPORT_FUNCTION_FROM_MODULE_ERROR_MESSAGE,
        INFO_MESSAGES,
        ARGUMENT_PARSER_STRINGS,
        FILENAME_FRIENDLY_DATE_FORMAT,
    )

    localedir = Path(__file__).parent / "locale"
    _ = configure_locale("pyutils", localedir)
except ImportError:
    print(
        "Warning: Could not import configure_locale from pyutils.locale_util. "
        "Ensure pyutils module is fully in the source location. "
        "Localization will not be available."
    )

    def _(text):
        """Fallback no-op localization function."""
        return text

    def configure_locale(domain: str, localedir: Path) -> Any:
        """Fallback no-op configure_locale function."""
        return _

    INFO_MESSAGES = {
        "update_count": "Scanned: {scanned}, Found: {found}",
        "calculating_hash": "Calculating hash for {file}",
        "hash_calculated": "Hash calculated for {file}: {file_hash}",
        "removed_duplicate_file": "Removed duplicate file: {file}",
        "create_directory": "Creating directory: {directory},",
    }

    VERBOSE_MESSAGES = {
        "renaming": "Renaming: {old_filename} to {new_filename}",
        "no_matching_files": "No matching files found.",
        "file_list": "Scan #{count}: {file}",
        "skipping_file": "Skipping {file} due to error in hash calculation: {error}",
        "earliest_unique": "Earliest unique file for hash {file_hash}: {file}",
        "unique_name_attempt": "Attempting unique filename: {new_file_path}",
        "final_unique_name": "Final unique filename: {new_file_path}",
    }

    ERROR_MESSAGES = {
        "access": "Error accessing file {file} in directory {root}: {e}",
        "hash": "Error calculating hash for file {file}: {e}",
        "rename": "Error renaming {file} to {new_name}: {e}",
        "directory_not_found": "The directory {directory} does not exist or is not a directory.",
        "error_removing_file": "Error removing file {file}: {e}",
        "create_directory": "Cannot create directory {directory}: {e}",
    }

    DRY_RUN_MESSAGES = {
        "dry_run_rename_file": "[Dry Run] Would rename: {file} to {unique_name}",
        "dry_run_remove_file": "[Dry Run] Would remove duplicate file: {file}",
        "dry_run_create_dir": "[Dry Run] Would create directory: {directory}",
    }

    FILENAME_FRIENDLY_DATE_FORMAT = "%Y-%m-%d-%H-%M-%S"

INFO_MESSAGES.update(
    {
        "update_count": "Scanned: {scanned}, Found: {found}",
        "calculating_hash": "Calculating hash for {file}",
        "hash_calculated": "Hash calculated for {file}: {file_hash}",
        "removed_duplicate_file": "Removed duplicate file: {file}",
    }
)

VERBOSE_MESSAGES.update(
    {
        "no_matching_files": "No matching files found.",
        "file_list": "Scan #{count}: {file}",
        "skipping_file": "Skipping {file} due to error in hash calculation: {error}",
        "earliest_unique": "Earliest unique file for hash {file_hash}: {file}",
        "unique_name_attempt": "Attempting unique filename: {new_file_path}",
        "final_unique_name": "Final unique filename: {new_file_path}",
    }
)

ERROR_MESSAGES.update(
    {
        "hash": "Error calculating hash for file {file}: {e}",
    }
)


ARGUMENT_PARSER_STRINGS.update(
    {
        "source_directory_help": "Directory to search for files.",
        "target_directory_help": "Directory to place renamed files.",
        "verbosity_help": "Increase verbosity of output.",
        "regex_help": "Regular expression to match files.",
        "commit_help": "Apply changes, overriding the dry-run default.",
    }
)

try:
    from pyutils.filesystem import (
        create_unique_filename,
        find_matching_files,
        get_earliest_unique_files,
    )
except ImportError as ie:
    error_message = (
        f'{IMPORT_FUNCTION_FROM_MODULE_ERROR_MESSAGE["import_error"].format(function="filesystem", module="pyutils")}.'
        f'{IMPORT_FUNCTION_FROM_MODULE_ERROR_MESSAGE["functionality_unavailable_present_tense"].format(functionality="clean_up_duplicates")}'
    )
    raise ImportError(error_message) from ie

SOURCE_DIRECTORY = Path.cwd()
TARGET_DIRECTORY = Path.cwd()

ARGUMENT_PARSER_EPILOG = {
    "examples": _(
        "Examples:\n"
        "  Find all .txt files in the current directory:\n"
        "    python clean_up_duplicates.py -r '.*\\.txt$'\n\n"
        "  Find all .jpg files in a specific directory:\n"
        "    python clean_up_duplicates.py -d /path/to/your/directory -r '.*\\.jpg$'\n\n"
        "  Commit the changes and rename files:\n"
        "    python clean_up_duplicates.py -d /home/user/files -r '.*\\.pdf$' --commit"
    ),
}

logger = getLogger(__name__)


def scan_directory_for_files(
    directory: Path, regex_pattern: str, verbosity: int
):
    """
    Scan the directory to find filenames matching the regex pattern.
    Parameters:
        directory (Path): Directory to scan.
        regex_pattern (str): Regular expression to match filenames.
        verbosity (int): Level of verbosity for logging.
    Returns:
        matching_files (list[Path]): List of matching files.
        total_files_scanned (int): Total number of files scanned.
        total_files_found (int): Total number of files matching the regex.
    """
    total_files_scanned = 0
    total_files_found = 0
    matching_files = []
    for is_file, path, matched in find_matching_files(
        directory, regex_pattern
    ):
        total_files_scanned += 1
        if matched and is_file:
            total_files_found += 1
            matching_files.append(path)
            if verbosity >= 1:
                logger.info(
                    VERBOSE_MESSAGES["file_list"].format(
                        count=len(matching_files), file=path
                    )
                )
    return matching_files, total_files_scanned, total_files_found


def compute_sha_sums(matching_files: list[Path]):
    """
    Establish the SHA sum of each matching file and identify the earliest file for each SHA sum.
    Parameters:
        matching_files (list[Path]): List of matching files.
    Returns:
        file_hash_map (dict[str, list[Path]]): Map of SHA sums to list of files.
        earliest_files (list[Path]): List of earliest files for each unique SHA sum.
    """
    file_hash_map, earliest_files = get_earliest_unique_files(matching_files)
    return file_hash_map, earliest_files


def rename_earliest_files(
    earliest_files: list[Path], target_directory: Path, dry_run: bool
):
    """
    Renames a list of files to a unique name within the target directory.

    Parameters:
        earliest_files (list[Path]): A list of Path objects representing the files to be renamed.
        target_directory (Path): The directory where the files will be renamed.
        dry_run (bool): If True, the renaming operations are logged but not actually performed.
    """
    existing_names: set[Path] = set()
    for file in earliest_files:
        unique_name = (
            target_directory
            / create_unique_filename(file, existing_names).name
        )
        if dry_run:
            logger.info(
                DRY_RUN_MESSAGES["dry_run_create_directory"].format(
                    directory=target_directory
                )
            )
            logger.info(
                DRY_RUN_MESSAGES["dry_run_rename_file"].format(
                    file=file, unique_name=unique_name
                )
            )
        else:
            try:
                logger.info(
                    INFO_MESSAGES["create_directory"].format(
                        directory=target_directory
                    )
                )
                target_directory.mkdir(parents=True, exist_ok=True)
            except OSError as err:
                logger.error(
                    ERROR_MESSAGES["create_directory"].format(
                        file=file, new_name=unique_name, e=err
                    )
                )
            try:
                logger.info(
                    VERBOSE_MESSAGES["renaming"].format(
                        file=file, unique_name=unique_name
                    )
                )
                file.rename(unique_name)
            except OSError as err:
                logger.error(
                    ERROR_MESSAGES["rename"].format(
                        old_filename=file, new_filename=unique_name, e=err
                    )
                )


def remove_duplicate_files(
    file_hash_map: dict[str, list[Path]], dry_run: bool
):
    """
    Remove all files except the earliest one of each filename sharing a SHA sum.
    Parameters:
        file_hash_map (dict[str, list[Path]]): Map of SHA sums to list of files.
        dry_run (bool): If True, only logs the actions that would be taken without making any changes.
    """
    for _file_hash, files in file_hash_map.items():
        earliest_file = min(files, key=lambda p: p.stat().st_mtime)
        for file in files:
            if file != earliest_file:
                if dry_run:
                    logger.info(
                        DRY_RUN_MESSAGES["dry_run_remove_file"].format(
                            file=file
                        )
                    )
                else:
                    try:
                        file.unlink()
                        logger.info(
                            INFO_MESSAGES["removed_duplicate_file"].format(
                                file=file
                            )
                        )
                    except OSError as err:
                        logger.error(
                            ERROR_MESSAGES["error_removing_file"].format(
                                file=file, e=err
                            )
                        )


def process_files(
    source_directory: Path = SOURCE_DIRECTORY,
    target_directory: Path = TARGET_DIRECTORY,
    regex_pattern: Optional[str] = None,
    dry_run: bool = True,
    verbosity: int = 0,
):
    """
    Processes files in a given directory that match a specified regex pattern and performs operations on them such as renaming and removing duplicates.
    Parameters:
    directory (Path): The path to the directory to process. Defaults to DEFAULT_DIRECTORY.
    regex_pattern (Optional[str]): The regex pattern to match files. This parameter is required.
    dry_run (bool): If set to True, no actions will be taken but the operations will be logged. Defaults to True.
    verbosity (int): The level of verbosity for logging and printing messages. Defaults to 0.
    """
    if not source_directory.is_dir():
        logger.error(
            ERROR_MESSAGES["directory_not_found"].format(
                directory=source_directory
            )
        )
        return
    if regex_pattern is None:
        logger.error("A regex pattern is required to process files.")
        return

    matching_files, total_files_scanned, total_files_found = (
        scan_directory_for_files(source_directory, regex_pattern, verbosity)
    )

    if verbosity >= 1:
        print(
            f"\r{INFO_MESSAGES['update_count'].format(scanned=total_files_scanned, found=total_files_found)}",
            end="",
        )
    print()

    if not matching_files:
        logger.info(VERBOSE_MESSAGES["no_matching_files"])
        return

    file_hash_map, earliest_files = compute_sha_sums(matching_files)
    rename_earliest_files(earliest_files, target_directory, dry_run)
    remove_duplicate_files(file_hash_map, dry_run)


def main():
    """
    Main function to parse command line arguments and process files.

    Parses arguments for directory, regex, commit flag, and verbosity level.
    Sets the logging configuration based on verbosity.
    Processes files in the specified directory according to the provided regex.

    Arguments:
    - -d, --directory: Directory to scan for files. Defaults to DEFAULT_DIRECTORY.
    - -r, --regex: Regular expression pattern to match files. This argument is required.
    - --commit: Commits the changes if this flag is specified.
    - -v, --verbose: Verbosity level of logging. Increases with each -v (0 for ERROR, 1 for INFO, 2 or more for DEBUG).

    Logging levels are set according to the verbosity level specified:
    - 0 for ERROR
    - 1 for INFO
    - 2 or more for DEBUG

    Initiates processing of files in the directory based on the regex pattern and commit flag.
    """
    parser = ArgumentParser(
        description=__doc__,
        epilog=ARGUMENT_PARSER_EPILOG["examples"],
        formatter_class=RawTextHelpFormatter,
    )
    parser.add_argument(
        "-s",
        "--source_directory",
        type=str,
        default=SOURCE_DIRECTORY,
        help=ARGUMENT_PARSER_STRINGS["source_directory_help"],
    )
    parser.add_argument(
        "-t",
        "--target_directory",
        type=str,
        default=TARGET_DIRECTORY,
        help=ARGUMENT_PARSER_STRINGS["target_directory_help"],
    )
    parser.add_argument(
        "-r",
        "--regex",
        type=str,
        required=True,
        help=ARGUMENT_PARSER_STRINGS["regex_help"],
    )
    parser.add_argument(
        "--commit",
        action="store_true",
        help=ARGUMENT_PARSER_STRINGS["commit_help"],
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        default=0,
        help=ARGUMENT_PARSER_STRINGS["verbosity_help"],
    )
    args = parser.parse_args()
    if args.verbose == 0:
        basicConfig(level=ERROR, format="%(message)s")
    elif args.verbose == 1:
        basicConfig(level=INFO, format="%(message)s")
    elif args.verbose >= 2:
        basicConfig(level=DEBUG, format="%(message)s")
    logger.setLevel(INFO)
    process_files(
        source_directory=Path(args.source_directory),
        target_directory=Path(args.target_directory),
        regex_pattern=args.regex,
        dry_run=not args.commit,
        verbosity=args.verbose,
    )


if __name__ == "__main__":
    main()
