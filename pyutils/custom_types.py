# -*- coding: utf-8 -*-
"""This module contains custom data types."""

# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

from typing import Any, Mapping, Sequence, TypeAlias

# JSON typing in Python is a mess. See https://github.com/python/typing/issues/182 (opened 2016)

JSON: TypeAlias = (
    dict[str, "JSON"] | list["JSON"] | str | int | float | bool | None
)
JSON_ro: TypeAlias = (
    Mapping[str, "JSON_ro"]
    | Sequence["JSON_ro"]
    | str
    | int
    | float
    | bool
    | None
)


def get_json_value(json_object: JSON, key: str) -> Any:
    """Get the value of a given key from a JSON object.

    Args:
        json_object (JSON): A JSON object that will be searched for the provided key.
        key (str): A string representing the key for which the value will be retrieved.

    Returns:
        Any: The value associated with the given key in the JSON object.

    Raises:
        ValueError: If the provided JSON object is not a dictionary.

    """
    try:
        return json_object.get(key)
    except AttributeError as err:
        raise ValueError(
            "The provided JSON object is not a dictionary."
        ) from err
