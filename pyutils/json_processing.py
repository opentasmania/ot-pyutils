# -*- coding: utf-8 -*-
"""This module contains code for processing json files."""

# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

from datetime import datetime
from json import dumps
from numbers import Real

from .exceptions import (
    JsonCustomObjectError,
    JsonDateError,
    JsonNumberPrecisionError,
)
from .custom_types import JSON


def is_jsonable(data: JSON) -> bool:
    """Check that data is JSON serializable.

    :param data: The data to be checked if it is JSON serializable.
    :return: Returns True if the data is JSON serializable, False otherwise.

    It handles different data types and recursively calls itself for nested data structures.
    It throws custom exceptions for certain types of data that are not JSON serializable.
    The method uses the `isinstance()` function to determine the data type.
    If the data is a dictionary, it iterates through the values of the dictionary.
    If the data is a list or tuple, it uses the data as is.
    Otherwise, it wraps the data in a list to iterate over.

    For each value in the iterable data, it checks for certain conditions and throws exceptions
     if the conditions are met. If the value is a dictionary or a list/tuple, it recursively calls
     itself to check the nested data structure. If the value is a `datetime` object, it raises a
     `JsonDateError` exception indicating that JSON serialization does not support `datetime` objects.
     If the value is a number with a precision greater than 15, it raises a `JsonNumberPrecisionError`
     exception indicating that JSON serialization does not support numbers with precision greater than 15.

    If none of the above conditions are met, it tries to serialize the value using `dumps()` function
     from `json` module. If it succeeds, it returns True indicating that the data is JSON serializable.
     If it fails with a TypeError, it raises a `JsonCustomObjectError` exception indicating that JSON
     serialization does not support custom objects.

    Note: The `JsonTypeHint` is a type hint used in the method signature to indicate that the `data`
     parameter must be JSON serializable.

    """
    if isinstance(data, dict):
        iter_data = list(data.values())
    elif isinstance(data, (list, tuple)):
        iter_data = data
    else:
        iter_data = [data]

    for val in iter_data:
        if isinstance(val, dict) or isinstance(val, (list, tuple)):
            is_jsonable(val)
        elif isinstance(val, datetime):
            raise JsonDateError(
                "JSON cannot serialize datetime.datetime objects"
            )
        elif isinstance(val, Real) and len(str(val).split(".")[1]) > 15:
            raise JsonNumberPrecisionError(
                "JSON cannot serialize numbers with precision greater than 15"
            )
        else:
            try:
                try:
                    if isinstance(val, (str, int, float)):
                        float(val)
                        string_is_float = True
                    else:
                        string_is_float = False
                except ValueError:
                    string_is_float = False

                if isinstance(val, str) and "." in val and string_is_float:
                    parts = val.split(".")
                    if len(parts[1]) > 15:
                        raise JsonNumberPrecisionError(
                            "JSON cannot serialize numbers with precision greater than 15"
                        )
                dumps(val)
                return True
            except TypeError as err:
                raise JsonCustomObjectError(
                    "JSON cannot serialize custom objects"
                ) from err
    return False
