# -*- coding: utf-8 -*-
"""Functions for detecting duplicate images in a Google Photos library."""
# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

import os

from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow  # type: ignore
from googleapiclient.discovery import build as build  # type: ignore
from googleapiclient.errors import HttpError as HttpError  # type: ignore
from requests import Request

# Replace with your actual credentials
SCOPES = ["https://www.googleapis.com/auth/photoslibrary.readonly"]
CREDENTIALS_FILE = "credentials.json"


def get_google_photos_service():
    """Builds and returns a Google Photos API service object."""
    try:
        service = build("photoslibrary", "v1", credentials=get_credentials())
        return service
    except HttpError as error:
        print(f"An HTTP error occurred: {error}")
        return None


def get_credentials():
    """Handles authentication with Google Photos API."""
    creds = None
    token_file = "token.json"
    if os.path.exists(token_file):
        creds = Credentials.from_authorized_user_file(token_file, SCOPES)
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                CREDENTIALS_FILE, SCOPES
            )
            creds = flow.run_local_server(port=0)
        with open(token_file, "w") as token:
            token.write(creds.to_json())
    return creds


def get_media_items(service):
    """Retrieves media items from Google Photos library."""
    results = service.mediaItems().list(pageSize=50).execute()
    media_items = results.get("mediaItems", [])
    next_page_token = results.get("nextPageToken", None)
    while next_page_token:
        results = (
            service.mediaItems()
            .list(pageSize=50, pageToken=next_page_token)
            .execute()
        )
        media_items.extend(results.get("mediaItems", []))
        next_page_token = results.get("nextPageToken", None)
    return media_items


def compare_metadata(media_item1, media_item2):
    """Compares metadata of two media items."""
    # Customize this function based on the specific metadata fields you want to compare
    # Example: Compare creation time, camera make, camera model
    metadata1 = media_item1.get("mediaMetadata", {})
    metadata2 = media_item2.get("mediaMetadata", {})

    if (
        metadata1.get("creationTime") == metadata2.get("creationTime")
        and metadata1.get("cameraMake") == metadata2.get("cameraMake")
        and metadata1.get("cameraModel") == metadata2.get("cameraModel")
    ):
        return True
    return False


def find_matching_dng(jpeg_item, media_items):
    """Finds the corresponding DNG file for a JPEG."""
    for item in media_items:
        if (
            item["mimeType"] == "image/dng"
            and item["filename"]
            == jpeg_item["filename"].replace(".jpg", ".dng")
            and compare_metadata(jpeg_item, item)
        ):
            return item
    return None


def main():
    """Main function to execute the script."""
    service = get_google_photos_service()
    if not service:
        return

    media_items = get_media_items(service)

    jpeg_items_to_delete = []
    for item in media_items:
        if item["mimeType"] == "image/jpeg":
            matching_dng = find_matching_dng(item, media_items)
            if matching_dng:
                jpeg_items_to_delete.append(item)

    # Print the list of JPEG items to be deleted (for review before actual deletion)
    for item in jpeg_items_to_delete:
        print(f"JPEG to delete: {item['filename']}")

    # Uncomment the following lines to actually delete the JPEG items
    # for item in jpeg_items_to_delete:
    #   service.mediaItems().delete(mediaItemId=item['id']).execute()


if __name__ == "__main__":
    main()
