# -*- coding: utf-8 -*-
"""This module contains custom data type information for IDEs."""

# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

from typing import TypeAlias, Mapping, Sequence, Any

JSON: TypeAlias = (
    dict[str, "JSON"] | list["JSON"] | str | int | float | bool | None
)
JSON_ro: TypeAlias = (
    Mapping[str, "JSON_ro"]
    | Sequence["JSON_ro"]
    | str
    | int
    | float
    | bool
    | None
)

def get_json_value(json_object: JSON, key: str) -> Any: ...
