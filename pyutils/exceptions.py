"""This module contains custom exception classes."""

# -*- coding: utf-8 -*-
# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

# ANSI escape codes for text color formatting
RED_TEXT = "\033[91m"
RESET_COLOR = "\033[0m"  # Reset text color to default


class RedTextException(Exception):
    """Display an error message in red text."""

    def __init__(self, message: str):
        """
        Initialize exception with red error message.

        :param message: The error message to be displayed.
        """
        self.message = message

    def __str__(self) -> str:
        """
        Return error message, formatted with red text.

        :return: The red error message.
        """
        return f"{RED_TEXT}{self.message}{RESET_COLOR}"


class ExistingFilesException(Exception):
    """
    Exception raised when there are existing files.

    :param message: Optional message explaining the reason for the exception.
                    Defaults to "Existing files."
    :type message: str, optional
    """

    def __init__(self, message="Existing files."):
        """
        Initialize an object of the class.

        :param message: A string representing an optional custom message.
        """
        self.message = message
        super().__init__(self.message)


class ModuleNotAvailableException(Exception):
    """
    Raised when a module cannot be installed or imported.

    :param message: The error message associated with the exception.
    :type message: str
    """

    def __init__(self, message="Module cannot be installed or imported."):
        """
        Class constructor.

        :param message: A string representing the error message.
        """
        self.message = message
        super().__init__(self.message)


class MaxRetriesReached(Exception):
    """
    Raised when a maximum number of retries has been exceeded.

    Parameters:
    - `retries` (optional): The number of retries that was reached.
    - `cause` (optional): The cause of the maximum retries being reached.

    Attributes:
    - `retries`: The number of retries that was reached.
    - `cause`: The cause of the maximum retries being reached.
    - `message`: A formatted error message that describes the maximum retries reached and the cause.
    """

    def __init__(self, retries="unknown number", cause=None):
        """
        Class constructor.

        :param retries: The maximum number of retries. Defaults to 3.
        :param cause: The cause of the maximum retries.
        :type cause: Any
        """
        self.retries = retries
        self.cause = cause
        if self.cause:
            cause_message = str(self.cause)
        else:
            cause_message = "no further information"
        self.message = f"Maximum number of retries ({self.retries}) has been exceeded. Cause: {cause_message}."
        super().__init__(self.message)


class JsonCustomObjectError(Exception):
    """Raised when the input JSON data contains a custom object."""

    def __init__(self, message="JSON data contains a custom object."):
        """
        Class constructor.

        :param message: A string representing the error message.
        """
        self.message = message
        super().__init__(self.message)


class JsonDateError(Exception):
    """Raised when the input JSON data contains a date/time object."""

    def __init__(self, message="JSON data contains a date/time object."):
        """
        Class constructor.

        :param message: A string representing the error message.
        """
        self.message = message
        super().__init__(self.message)


class JsonNumberPrecisionError(Exception):
    """Raised when the input JSON data contains a number with too high precision for Python's float."""

    def __init__(
        self,
        message="JSON data contains a number with too high precision for Python's float.",
    ):
        """
        Class constructor.

        :param message: A string representing the error message.
        """
        self.message = message
        super().__init__(self.message)
