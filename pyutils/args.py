# -*- coding: utf-8 -*-
"""pyutils argument handling."""

# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

import sys
from logging import getLogger

from .filesystem import default_directories


logger = getLogger(__name__)

default_name = "unnamed"


def print_help_message(
    default_root: str | None, default_config: str | None
) -> None:
    """Print help message."""
    print(
        f"Parameters defaults:\n"
        f"--name={default_name}\n"
        f"--root={default_root}\n"
        f"--config={default_config}\n"
        "Usage:\n"
        "--root=<root_path> --config=<config_path>"
    )


def assign_parameter_from_arguments() -> tuple[str, str, str]:
    """Assign values to name, project_root_path, project_config_path parameters based on command-line arguments.

    :return: A tuple containing the assigned values for project_name, project_root_path and project_config_path.
    :rtype: tuple[str, str, str]
    """
    args = dict(arg.split("=") for arg in sys.argv[1:] if "=" in arg)
    default_root, default_config = default_directories()

    if "--root" in args and not args["--root"]:
        print("Error: --root parameter is provided but no value is assigned.")
        print_help_message(default_root, default_config)
        raise SystemExit

    if "--config" in args and not args["--config"]:
        print(
            "Error: --config parameter is provided but no value is assigned."
        )
        print_help_message(default_root, default_config)
        raise SystemExit

    if "--name" in args and not args["--name"]:
        print("Error: --name parameter is provided but no value is assigned.")
        print_help_message(default_root, default_config)
        raise SystemExit

    project_name = args.get("--name", args.get("-n", default_name))
    project_root_path = args.get("--root", args.get("-r", default_root))
    project_config_path = args.get("--config", args.get("-c", default_config))

    if not project_root_path or not project_config_path:
        print("Error: Both --root and --config parameters must be provided.")
        print_help_message(default_root, default_config)
        raise SystemExit
    return project_name, project_root_path, project_config_path
