# -*- coding: utf-8 -*-
"""User interaction utilities."""

# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"


from click import Choice, prompt
from logging import getLogger, info


logger = getLogger(__name__)


def yes_no_action(
    prompt_text: str = "Do you want to continue?",
    yes_string: str = "You chose to continue.",
    no_string="You chose not to continue.",
) -> bool:
    """
    Asks the user for a yes or no input and returns a boolean value based on the response.

    Arguments:
    prompt_text (str): The question prompt presented to the user. Defaults to "Do you want to continue?".
    yes_string (str): The message displayed if the user confirms with 'y'. Defaults to "You chose to continue.".
    no_string (str): The message displayed if the user denies with 'n'. Defaults to "You chose not to continue.".

    Returns:
    bool: Returns True if user inputs 'y', False otherwise.
    """
    confirmation = prompt(
        text=prompt_text,
        default="y",
        show_default=True,
        type=Choice(["y", "n"], case_sensitive=False),
    )

    if confirmation.lower() == "y":
        info(f"{yes_string}")
        return True
    else:
        info(f"{no_string}")
        return False
