# -*- coding: utf-8 -*-
"""Filesystem utilities."""

# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

from collections import defaultdict
from datetime import datetime
from hashlib import sha512
from itertools import chain
from logging import debug, getLogger, info
from os import listdir, makedirs, remove, utime, walk
from os.path import (
    exists,
    expanduser,
    expandvars,
    isdir,
    isfile,
    join,
    relpath,
)
from pathlib import Path
from platform import system
from re import compile as re_compile
from shutil import copy2, rmtree
from subprocess import CalledProcessError, run
from typing import Dict, List, Optional, Tuple, Union

from pytz import utc

from pyutils.locale_util import (
    configure_locale,
    ERROR_MESSAGES,
    INFO_MESSAGES,
    VERBOSE_MESSAGES,
    FILENAME_FRIENDLY_DATE_FORMAT,
)
from pyutils.operating_system import get_os_type

localedir = Path(__file__).parent / "locale"
_ = configure_locale("pyutils", localedir)

DEFAULT_DIRECTORY = Path.cwd()

logger = getLogger(__name__)


def get_existing_files(directory: str) -> Union[List[str], None]:
    """Retrieve existing files from a directory.

    :param directory: The path of the directory to search for existing files.
    :return: A list of strings representing the names of the existing files in the given directory.
             If no files are found, None is returned.
    """
    return [f for f in listdir(directory) if isfile(join(directory, f))]


def get_existing_subdirs(directory: str) -> Union[List[str], None]:
    """Retrieve existing subdirectories from a directory.

    :param directory: The path of the directory to search for existing directories.
    :return: A list of strings representing the names of the existing directories in the given directory.
             If no directories are found, None is returned.
    """
    return [d for d in listdir(directory) if isdir(join(directory, d))]


def validate_filepath(filepath: str) -> bool:
    """Validate a filepath exists and is readable.

    Args:
        filepath: The path of the file to be validated.

    Raises:
        FileNotFoundError: If the file does not exist.
        PermissionError: If there is insufficient permission to read the file.

    Returns:
        bool: True if the file is valid, otherwise False.
    """
    file_path = Path(filepath)

    if not file_path.exists():
        raise FileNotFoundError(f"{filepath} does not exist.")

    if not file_path.is_file():
        raise FileNotFoundError(f"{filepath} is not a file.")

    try:
        with file_path.open("r"):
            pass
    except PermissionError as pe:
        raise PermissionError(
            f"Insufficient permission to read {filepath}"
        ) from pe

    return True


def default_directories() -> Tuple[Optional[str], Optional[str]]:
    """Return default directories for the current operating system.

    :return: A tuple containing the root directory and configuration directory.
         If the operating system is not supported, both values will be None.
    """
    posixish_directories = {"bsd", "linux", "macos", "micropython", "posix"}

    os_type = get_os_type()

    if os_type not in posixish_directories and os_type != "windows":
        raise ValueError("OS type not supported.")

    if os_type in posixish_directories:
        default_root_dir = expanduser(expandvars("~"))
    else:
        default_root_dir = expanduser(expandvars("%HOME%"))

    default_config_dir = expanduser(expandvars(f"{default_root_dir}/.config"))

    return default_root_dir, default_config_dir


def copy_directory_structure(source_dir, destination_dir):
    """
    Copy the directory structure from the source directory to the destination directory.

    Args:
        source_dir (str): The source directory to copy the structure from.
        destination_dir (str): The destination directory to create the structure in.

    Returns:
        None
    """
    if not isdir(source_dir):
        raise FileNotFoundError(
            f"The source directory does not exist: {source_dir}"
        )
    for root, _subdirs, files in walk(source_dir):
        destination_root = join(destination_dir, relpath(root, source_dir))

        makedirs(destination_root, exist_ok=True)

        for file in files:
            source_file = join(root, file)
            destination_file = join(destination_root, file)
            copy2(source_file, destination_file)


def clear_directory(dir_path):
    """
    Clears all files and subdirectories inside the specified directory.

    Parameters:
        dir_path (str): The path to the directory to be cleared.

    Raises:
        FileNotFoundError: If the specified directory does not exist.
        PermissionError: If there is a permission issue deleting a file or directory.

    Note:
        Directories and their contents are recursively deleted.
    """
    if not exists(dir_path):
        raise FileNotFoundError(f"Directory {dir_path} does not exist.")

    for filename in listdir(dir_path):
        file_path = join(dir_path, filename)
        try:
            if isdir(file_path):
                rmtree(file_path)
            elif isfile(file_path):
                remove(file_path)
        except PermissionError as pe:
            raise PermissionError(
                f"Failed to delete {file_path} due to permission error: {pe}"
            ) from pe
        except Exception as e:
            raise Exception(
                f"Failed to delete {file_path}. Reason: {e}"
            ) from e


def get_newest_file_timestamp(
    path_to_check: Path,
    include: Optional[List[Path]] = None,
    exclude: Optional[List[Path]] = None,
) -> datetime | None:
    """
    Retrieves the timestamp of the newest file or directory in the specified directory.
    Args:
        path_to_check (Path): The path to the directory to check for the newest file or directory.
        include (Optional[List[Path]]): List of Path objects to include. Defaults to None.
        exclude (Optional[List[Path]]): List of Path objects to exclude. Defaults to None.
    Returns:
        datetime | None: The UTC timezoned timestamp of the newest file or directory, or None if empty or an error occurs.
    Raises:
        FileNotFoundError: If the specified directory does not exist.
        NotADirectoryError: If the specified path is not a directory.
    """
    path_to_check = Path(path_to_check).resolve()
    if not path_to_check.exists():
        raise FileNotFoundError(
            f"The directory {path_to_check} does not exist"
        )
    if not path_to_check.is_dir():
        raise NotADirectoryError(
            f"The path {path_to_check} is not a directory"
        )

    latest_time = None
    include_set = {p.resolve() for p in chain(include or [])}
    exclude_set = {p.resolve() for p in chain(exclude or [])}

    for path_item in path_to_check.glob("**/*"):
        resolved_path = path_item.resolve()
        if resolved_path in exclude_set:
            continue
        if include and resolved_path not in include_set:
            continue
        item_time = datetime.fromtimestamp(
            resolved_path.stat().st_mtime, tz=utc
        )
        if latest_time is None or item_time > latest_time:
            latest_time = item_time

    return latest_time


def calculate_sha512(file_path: Path) -> str:
    """

    Calculates the SHA-512 hash of a file.

    Args:
        file_path (str): The path to the file for which the SHA-512 hash will be calculated.

    Returns:
        str: The SHA-512 hash of the file in hexadecimal format.
    """
    if not file_path.exists():
        raise FileNotFoundError(f"The file '{file_path}' does not exist.")

    if file_path.is_dir():
        raise IsADirectoryError(
            f"The path '{file_path}' is a directory, not a file."
        )

    sha512_hash = sha512()
    with open(file_path, "rb") as f:
        for block in iter(lambda: f.read(4096), b""):
            sha512_hash.update(block)

    return sha512_hash.hexdigest()


def cleanup_directory(directory):
    """Cleans up the directory by removing it if it exists."""
    if not isdir(directory):
        raise FileNotFoundError(
            f"The directory '{directory}' does not exist."
        )
    rmtree(directory)


def rename_file_on_latest_timestamp_in_directory(
    file_name: str, directory_name: str, prefix: str = ""
) -> str:
    """
    Renames a file based on the latest file timestamp within a specified directory.

    Args:
        file_name (str): The name of the file to be renamed.
        directory_name (str): The name of the directory to search for the latest file timestamp.
        prefix (str, optional): A prefix to add to the renamed file. Defaults to an empty string.

    Returns:
        str: The new filename with the latest timestamp from the directory and the optional prefix.
    """
    newest_file = get_newest_file_timestamp(Path(directory_name))

    if newest_file is None:
        raise ValueError("No files found in the specified directory.")

    newest_file = newest_file.astimezone(utc)
    utc_time = newest_file.strftime("%Y%m%d%H%M%S")
    new_filename = (
        f"{prefix}_{Path(file_name).stem}_{utc_time}{Path(file_name).suffix}"
    )
    return new_filename


def get_creation_time(file_path: Path) -> datetime:
    """
    Get the creation time of a file.
    :param file_path: Path to the file.
    :return: Datetime object representing the file's creation time.
    """
    if not file_path.exists():
        raise FileNotFoundError(f"The file at {file_path} does not exist.")
    if not file_path.is_file():
        raise ValueError(f"The path {file_path} is not a file.")

    try:
        timestamp = file_path.stat().st_mtime
    except OSError as e:
        raise OSError(
            f"Could not access the file at {file_path}. Reason: {e}"
        ) from e

    return datetime.fromtimestamp(timestamp)


def set_creation_time(file_path: Path, creation_time: datetime):
    """
    Set the creation time of a file.
    :param file_path: Path to the file.
    :param creation_time: Datetime object representing the new creation time.
    """

    current_system = system()
    if current_system not in {"Darwin", "Linux"}:
        raise NotImplementedError(
            f"Setting creation time is not supported on this platform: {current_system}"
        )

    if not file_path.exists():
        raise FileNotFoundError(f"The file at {file_path} does not exist.")
    if not file_path.is_file():
        raise ValueError(f"The path {file_path} is not a file.")

    try:
        timestamp = creation_time.timestamp()
        utime(file_path, (timestamp, timestamp))
    except Exception as e:
        raise OSError(
            f"Could not set the creation time for the file at {file_path}. Reason: {e}"
        ) from e


def discover_files_checksums(
    root_dir: Path, filename: Path
) -> Dict[str, List[Tuple[Path, datetime]]]:
    """
    :param root_dir: The root directory to search within.
    :param filename: The name of the file to search for.
    :return: A dictionary mapping SHA-512 checksums to lists of tuples,
             where each tuple contains a Path object representing the file's location
             and a datetime object representing the file's creation time.
    """
    sha512_map: Dict[str, List[Tuple[Path, datetime]]] = defaultdict(list)
    for found_filename in root_dir.glob(f"**/{filename.name}"):
        debug(f"Found filename: {found_filename}")
        if found_filename.is_file():
            file_checksum = calculate_sha512(found_filename)
            debug(f"File checksum: {file_checksum}")
            if file_checksum:
                creation_time = get_creation_time(found_filename)
                debug(f"File creation time: {creation_time}")
                sha512_map[file_checksum].append(
                    (found_filename, creation_time)
                )
    debug(f"SHA512 map: {sha512_map}")
    return sha512_map


def compare_directory_timestamps(
    source_directory_path: Path,
    target_directory_path: Path,
    exclusions: list,
) -> bool:
    """
    :param source_directory_path: Name of the zip file to extract.
    :param temp_path: Temporary directory path where the archive will be extracted.
    :param exclusions: List of directories to exclude from timestamp comparison.
    :param target_directory_path: Path to the repository for comparison.
    :return: True if the newest file timestamp in the extracted archive is more recent than the newest file timestamp in the repository; otherwise, False.
    """

    exclusion_paths = [
        source_directory_path / Path(exclusion) for exclusion in exclusions
    ]
    source_directory_newest_timestamp = get_newest_file_timestamp(
        source_directory_path, exclusion_paths
    )
    target_directory_newest_timestamp = get_newest_file_timestamp(
        target_directory_path, exclude=exclusions
    )
    if target_directory_newest_timestamp is None:
        return True
    return (
        source_directory_newest_timestamp is not None
        and source_directory_newest_timestamp
        > target_directory_newest_timestamp
    )


def rsync_as_archive(
    source_directory: Path, destination_directory: Path, dry_run: bool = True
) -> None:
    """
    Synchronize the contents of two directories using rsync and log actions during a dry run.

    Parameters:
    - source_directory (Path): The directory to copy from.
    - destination_directory (Path): The directory to copy to.
    - dry_run (bool): If True, log the actions instead of performing them. Default is True.

    Raises:
    - FileNotFoundError: If the source or destination directory does not exist.
    - RuntimeError: If the rsync command fails.

    """

    def log_directory_contents(src_path: Path, dest_path: Path):
        """Recursively log contents of directories for dry run."""
        for item in src_path.iterdir():
            destination = dest_path / item.name
            if item.is_dir():
                info(f"Would copy directory {item} to {destination}")
                log_directory_contents(item, destination)
            else:
                info(f"Would copy file {item} to {destination}")

    if not source_directory.exists():
        raise FileNotFoundError(f"Source {source_directory} does not exist.")
    if not destination_directory.exists():
        raise FileNotFoundError(
            f"Destination {destination_directory} does not exist."
        )
    if dry_run:
        log_directory_contents(source_directory, destination_directory)
    else:
        try:
            rsync_command = [
                "rsync",
                "--archive",
                "--verbose",
                "--perms",
                "--partial",
                "--progress",
                str(source_directory) + "/",
                str(destination_directory) + "/",
            ]
            info(f"rsync command: {' '.join(rsync_command)}")
            run(rsync_command, check=True)
        except CalledProcessError as cpe:
            raise RuntimeError(
                f"Failed to rsync {source_directory} to {destination_directory}: {cpe}"
            ) from cpe


def find_matching_files(directory: Path, regex_pattern: str):
    """Finds files matching a given regular expression within a directory.
    Args:
        directory (Path): Path to the directory to search in.
        regex_pattern (str): Regular expression to match files.
    Yields:
        Path: Full path of matching files.
    """
    regex = re_compile(regex_pattern)
    for path in directory.rglob("*"):
        yield path.is_file(), path, regex.match(path.name)


def get_earliest_unique_files(
    files: list[Path],
) -> tuple[dict[str, list[Path]], list[Path]]:
    """Identify the earliest of each unique file based on its SHA-512 hash.
    Args:
        files (list of Path): List of file paths.
    Returns:
        tuple: Dictionary of file hashes and their corresponding paths, and list of earliest unique file paths.
    """
    file_hash_map = defaultdict(list)
    for file in files:
        print(
            INFO_MESSAGES["calculating_hash"].format(file=file)
        )  # Ensure calculation message is always shown
        try:
            file_hash = calculate_sha512(file)
            file_hash_map[file_hash].append(file)
            print(
                INFO_MESSAGES["hash_calculated"].format(
                    file=file, file_hash=file_hash
                )
            )  # Ensure hash result is always shown
        except (OSError, IOError) as err:
            logger.error(ERROR_MESSAGES["hash"].format(file=file, e=err))
            logger.error(
                VERBOSE_MESSAGES["skipping_file"].format(file=file, error=err)
            )
    earliest_files = [
        min(files, key=lambda p: p.stat().st_mtime)
        for file_hash, files in file_hash_map.items()
    ]
    for file_hash, file in zip(file_hash_map.keys(), earliest_files):
        logger.info(
            VERBOSE_MESSAGES["earliest_unique"].format(
                file=file, file_hash=file_hash
            )
        )
    return file_hash_map, earliest_files


def create_unique_filename(
    file_path: Path, existing_files_set: set[Path]
) -> Path:
    """Generate a unique filename by appending its timestamp and avoiding conflicts.
    Args:
        file_path (Path): Original file path.
        existing_files_set (set): Set of strings containing existing file names.
    Returns:
        Path: Unique file path.
    """
    timestamp = datetime.fromtimestamp(file_path.stat().st_mtime).strftime(
        FILENAME_FRIENDLY_DATE_FORMAT
    )
    dir_name, name, ext = file_path.parent, file_path.stem, file_path.suffix
    new_name = f"{name}_{timestamp}{ext}"
    new_file_path = dir_name / new_name
    counter = 1
    while new_file_path in existing_files_set or new_file_path.exists():
        logger.info(
            VERBOSE_MESSAGES["unique_name_attempt"].format(
                new_file_path=new_file_path
            )
        )
        new_name = f"{name}_{timestamp}_{counter}{ext}"
        new_file_path = dir_name / new_name
        counter += 1
    existing_files_set.add(new_file_path)
    logger.info(
        VERBOSE_MESSAGES["final_unique_name"].format(
            new_file_path=new_file_path
        )
    )
    return new_file_path
