# -*- coding: utf-8 -*-
"""Functions for project handling."""

# Copyright Peter Lawler <relwalretep@gmail.com>
# GNU Affero (AGPL) v3
__author__ = "Peter Lawler <relwalretep@gmail.com>"
__copyright__ = "Copyright Peter Lawler <relwalretep@gmail.com>"
__licence__ = "GNU Affero (AGPL) v3"

from importlib.util import find_spec
from os import getcwd
from os.path import dirname, isfile, join
from typing import Optional


def find_project_root():
    """
    Return root directory of project based on presence of a 'requirements.txt' or 'pyproject.toml' file.

    This method iteratively checks the current directory and its parent directories until it finds the project root.
    The project root is determined based on the presence a LICENCE file.

    Returns:
        str: The absolute path of the project root directory.

    Raises:
        FileNotFoundError: If the project root directory cannot be found.
    """
    current_path = getcwd()

    while True:
        if isfile(join(current_path, "LICENCE")):
            return current_path
        parent_path = dirname(current_path)
        if parent_path == current_path:
            raise FileNotFoundError(
                "Could not find the directory with accompanying 'LICENCE'"
            )
        current_path = parent_path


def get_package_root(package_name: str) -> Optional[str]:
    """Find the root directory of a package.

    :param package_name: The name of the package.
    :return: The root directory of the package as a string, or None if the package is not found.
    :rtype: str or None

    :raises TypeError: If package_name is None.
    """
    if package_name is None:
        raise TypeError("package_name cannot be None")
    spec = find_spec(package_name)
    if spec is None or spec.submodule_search_locations is None:
        return None
    return spec.submodule_search_locations[0]
