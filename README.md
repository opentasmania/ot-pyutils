# Open Tasmania Python Utilities

## Name
Python Utilities

## Description

Small utility Python modules for projects.

## Badges

![Pipeline](https://gitlab.com/opentasmania/pyutils/badges/main/pipeline.svg)
![Coverage](https://gitlab.com/opentasmania/pyutils/badges/main/coverage.svg)

## Installation

#### requirements.txt:
Add the following to your requirements.txt file  
`pyutils @ git+https://gitlab.com/opentasmania/pyutils.git ; python_version >= "3.12" and python_version < "4.0"`

#### poetry:
In the **[tool.poetry.dependencies]** section of your pyproject.toml  
`pyutils = { git = "https://gitlab.com/opentasmania/pyutils.git" }`

## Dependencies
| Requirement                                               | Version     |  
|-----------------------------------------------------------|-------------|
| [python](https://python.org)                              | >=3.12,<4.0 |  
| [pytest](https://pytest.org/)                             | ^8.2        |
| [pytest-mock](https://github.com/pytest-dev/pytest-mock/) | ^3          | 
| [requests](https://github.com/psf/requests)               | ^2          |

## Support

Don’t hesitate to use the [issues board](https://gitlab.com/opentasmania/pyutils/-/issues) for asking
information.

## Roadmap

See [Project Status](#project-status)

## Contributing

[Merge requests](https://gitlab.com/opentasmania/pyutils/-/merge_requests) always welcome.

## Authors and acknowledgment

* Peter Lawler
* Creators of Python

## License

GNU Affero (AGPL) v3

## Project status

Active development.  

Much of the code here originates in other software, where it existed in some different variations. 
This package consolidates those versions and provides an easier maintenance path for development.
